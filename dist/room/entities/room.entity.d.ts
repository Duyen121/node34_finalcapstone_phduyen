export declare class Room {
    id?: number;
    room_name: string;
    location_id: number;
    cost: number;
    desc: string;
    image?: string;
    bed_num: number;
    bathroom_num: number;
    customer_per_room: number;
    bedroom_num: number;
    has_washing_machine: 'true' | 'false';
    has_iron: 'true' | 'false';
    has_tv: 'true' | 'false';
    has_air_conditioner: 'true' | 'false';
    has_wifi: 'true' | 'false';
    has_stove: 'true' | 'false';
    has_parking: 'true' | 'false';
    has_pool: 'true' | 'false';
}
