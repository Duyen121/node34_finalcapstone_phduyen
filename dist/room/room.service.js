"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RoomService = void 0;
const common_1 = require("@nestjs/common");
const prisma_service_1 = require("../prisma/prisma.service");
let RoomService = class RoomService {
    constructor(prisma) {
        this.prisma = prisma;
    }
    async findAll(location_id, searchKeyword, pageSize, pageIndex) {
        let data = await this.prisma.rooms.findMany({
            where: {
                OR: searchKeyword?.trim()
                    ? [
                        { room_name: { contains: searchKeyword } },
                        {
                            locations: {
                                OR: [
                                    { address: { contains: searchKeyword } },
                                    { city: { contains: searchKeyword } },
                                    { country: { contains: searchKeyword } },
                                ],
                            },
                        },
                    ]
                    : undefined,
                location_id: location_id || undefined,
            },
            include: {
                locations: {
                    select: {
                        address: true,
                        city: true,
                        country: true,
                    },
                },
            },
            skip: pageSize * (pageIndex - 1) || undefined,
            take: pageSize || undefined,
        });
        return data;
    }
    async findOne(id) {
        let room = await this.prisma.rooms.findUnique({
            where: { id },
        });
        return room;
    }
    async create(data) {
        let room = await this.prisma.rooms.create({
            data,
            include: {
                locations: {
                    select: {
                        address: true,
                        city: true,
                        country: true,
                    },
                },
            },
        });
        return room;
    }
    async update(id, data) {
        let room = await this.prisma.rooms.update({
            where: { id },
            data,
            include: {
                locations: {
                    select: {
                        address: true,
                        city: true,
                        country: true,
                    },
                },
            },
        });
        return room;
    }
    async remove(id) {
        let room = await this.prisma.rooms.delete({ where: { id } });
        return room;
    }
};
exports.RoomService = RoomService;
exports.RoomService = RoomService = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [prisma_service_1.PrismaService])
], RoomService);
//# sourceMappingURL=room.service.js.map