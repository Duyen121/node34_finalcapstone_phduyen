import { PrismaService } from "src/prisma/prisma.service";
import { Room } from "./entities/room.entity";
import { CreateRoomDto } from "./dto/create-room.dto";
export declare class RoomService {
    private prisma;
    constructor(prisma: PrismaService);
    findAll(location_id: number, searchKeyword: string, pageSize: number, pageIndex: number): Promise<Room[]>;
    findOne(id: number): Promise<Room>;
    create(data: any): Promise<CreateRoomDto>;
    update(id: number, data: any): Promise<any>;
    remove(id: number): Promise<Room>;
}
