"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UpdateRoomDto = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_transformer_1 = require("class-transformer");
class UpdateRoomDto {
}
exports.UpdateRoomDto = UpdateRoomDto;
__decorate([
    (0, swagger_1.ApiPropertyOptional)(),
    __metadata("design:type", String)
], UpdateRoomDto.prototype, "room_name", void 0);
__decorate([
    (0, swagger_1.ApiPropertyOptional)(),
    (0, class_transformer_1.Type)(() => Number),
    __metadata("design:type", Number)
], UpdateRoomDto.prototype, "location_id", void 0);
__decorate([
    (0, swagger_1.ApiPropertyOptional)(),
    (0, class_transformer_1.Type)(() => Number),
    __metadata("design:type", Number)
], UpdateRoomDto.prototype, "cost", void 0);
__decorate([
    (0, swagger_1.ApiPropertyOptional)(),
    __metadata("design:type", String)
], UpdateRoomDto.prototype, "desc", void 0);
__decorate([
    (0, swagger_1.ApiPropertyOptional)(),
    (0, class_transformer_1.Type)(() => Number),
    __metadata("design:type", Number)
], UpdateRoomDto.prototype, "bed_num", void 0);
__decorate([
    (0, swagger_1.ApiPropertyOptional)(),
    (0, class_transformer_1.Type)(() => Number),
    __metadata("design:type", Number)
], UpdateRoomDto.prototype, "bathroom_num", void 0);
__decorate([
    (0, swagger_1.ApiPropertyOptional)(),
    (0, class_transformer_1.Type)(() => Number),
    __metadata("design:type", Number)
], UpdateRoomDto.prototype, "customer_per_room", void 0);
__decorate([
    (0, swagger_1.ApiPropertyOptional)(),
    (0, class_transformer_1.Type)(() => Number),
    __metadata("design:type", Number)
], UpdateRoomDto.prototype, "bedroom_num", void 0);
__decorate([
    (0, swagger_1.ApiPropertyOptional)({ enum: ["true", "false"] }),
    __metadata("design:type", String)
], UpdateRoomDto.prototype, "has_washing_machine", void 0);
__decorate([
    (0, swagger_1.ApiPropertyOptional)({ enum: ["true", "false"] }),
    __metadata("design:type", String)
], UpdateRoomDto.prototype, "has_iron", void 0);
__decorate([
    (0, swagger_1.ApiPropertyOptional)({ enum: ["true", "false"] }),
    __metadata("design:type", String)
], UpdateRoomDto.prototype, "has_tv", void 0);
__decorate([
    (0, swagger_1.ApiPropertyOptional)({ enum: ["true", "false"] }),
    __metadata("design:type", String)
], UpdateRoomDto.prototype, "has_air_conditioner", void 0);
__decorate([
    (0, swagger_1.ApiPropertyOptional)({ enum: ["true", "false"] }),
    __metadata("design:type", String)
], UpdateRoomDto.prototype, "has_wifi", void 0);
__decorate([
    (0, swagger_1.ApiPropertyOptional)({ enum: ["true", "false"] }),
    __metadata("design:type", String)
], UpdateRoomDto.prototype, "has_stove", void 0);
__decorate([
    (0, swagger_1.ApiPropertyOptional)({ enum: ["true", "false"] }),
    __metadata("design:type", String)
], UpdateRoomDto.prototype, "has_parking", void 0);
__decorate([
    (0, swagger_1.ApiPropertyOptional)({ enum: ["true", "false"] }),
    __metadata("design:type", String)
], UpdateRoomDto.prototype, "has_pool", void 0);
//# sourceMappingURL=update-room.dto.js.map