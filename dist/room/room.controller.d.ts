/// <reference types="multer" />
import { RoomService } from "./room.service";
import { CreateRoomDto } from "./dto/create-room.dto";
import { UpdateRoomDto } from "./dto/update-room.dto";
export declare class RoomController {
    private readonly roomService;
    constructor(roomService: RoomService);
    get(id: string, locationId: string, searchKeyword: string, pageSize: string, pageIndex: string): Promise<{
        message: string;
        data: Record<string, any>;
    }>;
    create(data: CreateRoomDto): Promise<{
        message: string;
        data: Record<string, any>;
    }>;
    update(id: string, data: UpdateRoomDto): Promise<{
        message: string;
        data: Record<string, any>;
    }>;
    uploadAvatar(roomId: string, file: Express.Multer.File): Promise<{
        message: string;
    }>;
    delete(id: string): Promise<{
        message: string;
    }>;
}
