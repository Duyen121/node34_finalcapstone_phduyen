"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RoomController = void 0;
const common_1 = require("@nestjs/common");
const room_service_1 = require("./room.service");
const swagger_1 = require("@nestjs/swagger");
const class_transformer_1 = require("class-transformer");
const room_entity_1 = require("./entities/room.entity");
const create_room_dto_1 = require("./dto/create-room.dto");
const update_room_dto_1 = require("./dto/update-room.dto");
const local_auth_guard_1 = require("../auth/guards/local-auth.guard");
const platform_express_1 = require("@nestjs/platform-express");
const compress_image_pipe_1 = require("../pipes/compress-image.pipe");
const role_decorator_1 = require("../auth/decorators/role.decorator");
const user_enum_1 = require("../user/user.enum");
const upload_image_dto_1 = require("./dto/upload-image.dto");
let RoomController = class RoomController {
    constructor(roomService) {
        this.roomService = roomService;
    }
    async get(id, locationId, searchKeyword, pageSize, pageIndex) {
        try {
            let data;
            if (id) {
                data = await this.roomService.findOne(+id);
            }
            else {
                data = await this.roomService.findAll(+locationId, searchKeyword, +pageSize, +pageIndex);
            }
            if (data?.length === 0 || !data) {
                throw new common_1.HttpException("No result found", 200);
            }
            else {
                return {
                    message: "Successfully!",
                    data: (0, class_transformer_1.instanceToPlain)((0, class_transformer_1.plainToClass)(room_entity_1.Room, data)),
                };
            }
        }
        catch (err) {
            throw err || new common_1.InternalServerErrorException();
        }
    }
    async create(data) {
        try {
            let room = await this.roomService.create({
                ...data,
                image: "",
            });
            return {
                message: "Successfully created!",
                data: (0, class_transformer_1.instanceToPlain)((0, class_transformer_1.plainToClass)(room_entity_1.Room, room))
            };
        }
        catch (err) {
            throw err || new common_1.InternalServerErrorException();
        }
    }
    async update(id, data) {
        try {
            let room = await this.roomService.update(+id, {
                ...data,
                image: "",
            });
            return {
                message: "Successfully updated!",
                data: (0, class_transformer_1.instanceToPlain)((0, class_transformer_1.plainToClass)(room_entity_1.Room, room)),
            };
        }
        catch (err) {
            throw err || new common_1.InternalServerErrorException();
        }
    }
    async uploadAvatar(roomId, file) {
        if (!file) {
            throw new common_1.HttpException("Please upload room image!", 422);
        }
        let data = await this.roomService.findOne(+roomId);
        await this.roomService.update(+roomId, {
            ...data,
            image: data?.image !== null ? file && file?.filename : null,
        });
        return { message: "Successfully uploaded room image!" };
    }
    async delete(id) {
        try {
            await this.roomService.remove(+id);
            return { message: "Successfully deleted!" };
        }
        catch (err) {
            throw err || new common_1.InternalServerErrorException();
        }
    }
};
exports.RoomController = RoomController;
__decorate([
    (0, swagger_1.ApiQuery)({ name: "id", required: false }),
    (0, swagger_1.ApiQuery)({ name: "locationId", required: false }),
    (0, swagger_1.ApiQuery)({ name: "search", required: false }),
    (0, swagger_1.ApiQuery)({ name: "pageSize", required: false }),
    (0, swagger_1.ApiQuery)({ name: "pageIndex", required: false }),
    (0, common_1.Get)(),
    __param(0, (0, common_1.Query)("id")),
    __param(1, (0, common_1.Query)("locationId")),
    __param(2, (0, common_1.Query)("search")),
    __param(3, (0, common_1.Query)("pageSize")),
    __param(4, (0, common_1.Query)("pageIndex")),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, String, String, String, String]),
    __metadata("design:returntype", Promise)
], RoomController.prototype, "get", null);
__decorate([
    (0, role_decorator_1.Roles)(user_enum_1.UserRole.ADMIN),
    (0, swagger_1.ApiConsumes)("multipart/form-data"),
    (0, swagger_1.ApiBody)({
        type: create_room_dto_1.CreateRoomDto,
    }),
    (0, common_1.Post)("new-room"),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [create_room_dto_1.CreateRoomDto]),
    __metadata("design:returntype", Promise)
], RoomController.prototype, "create", null);
__decorate([
    (0, role_decorator_1.Roles)(user_enum_1.UserRole.ADMIN),
    (0, swagger_1.ApiConsumes)("multipart/form-data"),
    (0, swagger_1.ApiBody)({
        type: update_room_dto_1.UpdateRoomDto,
    }),
    (0, common_1.Put)("update/:id"),
    __param(0, (0, common_1.Param)("id")),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, update_room_dto_1.UpdateRoomDto]),
    __metadata("design:returntype", Promise)
], RoomController.prototype, "update", null);
__decorate([
    (0, role_decorator_1.Roles)(user_enum_1.UserRole.ADMIN),
    (0, swagger_1.ApiConsumes)("multipart/form-data"),
    (0, swagger_1.ApiQuery)({ name: "roomId", required: true }),
    (0, swagger_1.ApiBody)({
        type: upload_image_dto_1.UploadImageDto,
    }),
    (0, common_1.UseInterceptors)((0, platform_express_1.FileInterceptor)("image")),
    (0, common_1.Post)("upload-image"),
    __param(0, (0, common_1.Query)("roomId")),
    __param(1, (0, common_1.UploadedFile)(compress_image_pipe_1.CompressImagePipe)),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object]),
    __metadata("design:returntype", Promise)
], RoomController.prototype, "uploadAvatar", null);
__decorate([
    (0, role_decorator_1.Roles)(user_enum_1.UserRole.ADMIN),
    (0, common_1.Delete)("delete/:id"),
    __param(0, (0, common_1.Param)("id")),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], RoomController.prototype, "delete", null);
exports.RoomController = RoomController = __decorate([
    (0, common_1.UseGuards)(local_auth_guard_1.LocalAuthGuard),
    (0, swagger_1.ApiTags)("Room"),
    (0, common_1.Controller)("room"),
    __metadata("design:paramtypes", [room_service_1.RoomService])
], RoomController);
//# sourceMappingURL=room.controller.js.map