export declare class CreateLocationDto {
    id?: number;
    address: string;
    city: string;
    country: string;
    photo?: string;
}
