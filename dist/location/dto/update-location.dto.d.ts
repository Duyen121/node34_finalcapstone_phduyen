export declare class UpdateLocationDto {
    address: string;
    city: string;
    country: string;
    photo?: string;
}
