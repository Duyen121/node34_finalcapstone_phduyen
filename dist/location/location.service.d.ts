import { PrismaService } from "../prisma/prisma.service";
import { Location } from "./entities/location.entities";
import { CreateLocationDto } from "./dto/create-location.dto";
export declare class LocationService {
    private prisma;
    constructor(prisma: PrismaService);
    findAll(searchKeyword: string, pageSize: number, pageIndex: number): Promise<Location[]>;
    findOne(id: number): Promise<Location>;
    create(data: any): Promise<CreateLocationDto>;
    update(id: number, data: any): Promise<Location>;
    remove(id: number): Promise<Location>;
}
