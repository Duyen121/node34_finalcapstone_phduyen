/// <reference types="multer" />
import { LocationService } from "./location.service";
import { CreateLocationDto } from "./dto/create-location.dto";
import { UpdateLocationDto } from "./dto/update-location.dto";
export declare class LocationController {
    private readonly locationService;
    constructor(locationService: LocationService);
    get(id: string, searchKeyword: string, pageSize: string, pageIndex: string): Promise<{
        message: string;
        data: Record<string, any>;
    }>;
    create(data: CreateLocationDto, file: Express.Multer.File): Promise<{
        message: string;
        data: Record<string, any>;
    }>;
    update(id: string, data: UpdateLocationDto, file: Express.Multer.File): Promise<{
        message: string;
        data: UpdateLocationDto;
    }>;
    delete(id: number): Promise<{
        message: string;
    }>;
}
