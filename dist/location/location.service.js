"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.LocationService = void 0;
const common_1 = require("@nestjs/common");
const prisma_service_1 = require("../prisma/prisma.service");
let LocationService = class LocationService {
    constructor(prisma) {
        this.prisma = prisma;
    }
    async findAll(searchKeyword, pageSize, pageIndex) {
        const data = await this.prisma.locations.findMany({
            where: {
                OR: searchKeyword?.trim()
                    ? [
                        { address: { contains: searchKeyword || undefined } },
                        { city: { contains: searchKeyword || undefined } },
                        { country: { contains: searchKeyword || undefined } },
                    ]
                    : undefined,
            },
            skip: pageSize * (pageIndex - 1) || undefined,
            take: pageSize || undefined,
        });
        return data;
    }
    async findOne(id) {
        const location = await this.prisma.locations.findUnique({
            where: { id },
        });
        return location;
    }
    async create(data) {
        const location = await this.prisma.locations.create({ data });
        return location;
    }
    async update(id, data) {
        const location = await this.prisma.locations.update({
            where: { id },
            data,
        });
        return location;
    }
    async remove(id) {
        const location = await this.prisma.locations.delete({
            where: { id },
        });
        return location;
    }
};
exports.LocationService = LocationService;
exports.LocationService = LocationService = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [prisma_service_1.PrismaService])
], LocationService);
//# sourceMappingURL=location.service.js.map