"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.LocationModule = void 0;
const common_1 = require("@nestjs/common");
const location_service_1 = require("./location.service");
const location_controller_1 = require("./location.controller");
const local_strategy_1 = require("../auth/strategies/local.strategy");
const user_service_1 = require("../user/user.service");
const platform_express_1 = require("@nestjs/platform-express");
const multer_1 = require("multer");
let LocationModule = class LocationModule {
};
exports.LocationModule = LocationModule;
exports.LocationModule = LocationModule = __decorate([
    (0, common_1.Module)({
        imports: [
            platform_express_1.MulterModule.register({
                storage: (0, multer_1.diskStorage)({
                    destination: process.cwd() + "/public/img/location-photo",
                    filename: (req, file, callback) => callback(null, new Date().getTime() + "_" + file.originalname),
                }),
            }),
        ],
        controllers: [location_controller_1.LocationController],
        providers: [location_service_1.LocationService, local_strategy_1.LocalStrategy, user_service_1.UserService]
    })
], LocationModule);
//# sourceMappingURL=location.module.js.map