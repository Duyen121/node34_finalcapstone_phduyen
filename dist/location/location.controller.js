"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.LocationController = void 0;
const common_1 = require("@nestjs/common");
const location_service_1 = require("./location.service");
const swagger_1 = require("@nestjs/swagger");
const check_existence_pipe_1 = require("../pipes/check-existence.pipe");
const create_location_dto_1 = require("./dto/create-location.dto");
const location_entities_1 = require("./entities/location.entities");
const class_transformer_1 = require("class-transformer");
const update_location_dto_1 = require("./dto/update-location.dto");
const platform_express_1 = require("@nestjs/platform-express");
const compress_image_pipe_1 = require("../pipes/compress-image.pipe");
const local_auth_guard_1 = require("../auth/guards/local-auth.guard");
const role_decorator_1 = require("../auth/decorators/role.decorator");
const user_enum_1 = require("../user/user.enum");
let LocationController = class LocationController {
    constructor(locationService) {
        this.locationService = locationService;
    }
    async get(id, searchKeyword, pageSize, pageIndex) {
        try {
            let data;
            if (id) {
                data = await this.locationService.findOne(+id);
            }
            else {
                data = await this.locationService.findAll(searchKeyword, +pageSize, +pageIndex);
            }
            if (data?.length === 0 || !data) {
                throw new common_1.HttpException("No result found", 200);
            }
            else {
                return {
                    message: "Successfully!",
                    data: (0, class_transformer_1.instanceToPlain)((0, class_transformer_1.plainToClass)(location_entities_1.Location, data)),
                };
            }
        }
        catch (err) {
            throw err || new common_1.InternalServerErrorException();
        }
    }
    async create(data, file) {
        try {
            let location = await this.locationService.create({
                ...data,
                photo: data?.photo !== null ? file && file?.filename : null,
            });
            return {
                message: "Successfully created!",
                data: (0, class_transformer_1.instanceToPlain)((0, class_transformer_1.plainToClass)(create_location_dto_1.CreateLocationDto, location), {}),
            };
        }
        catch (err) {
            throw err || new common_1.InternalServerErrorException();
        }
    }
    async update(id, data, file) {
        try {
            const updatedLocation = await this.locationService.update(+id, {
                ...data,
                photo: data?.photo !== null ? file && file?.filename : null,
            });
            return {
                message: "Successfully updated!",
                data: updatedLocation,
            };
        }
        catch (err) {
            throw err || new common_1.InternalServerErrorException();
        }
    }
    async delete(id) {
        try {
            await this.locationService.remove(+id);
            return { message: "Successfully deleted!" };
        }
        catch (err) {
            throw err || new common_1.InternalServerErrorException();
        }
    }
};
exports.LocationController = LocationController;
__decorate([
    (0, swagger_1.ApiQuery)({ name: "id", required: false }),
    (0, swagger_1.ApiQuery)({ name: "search", required: false }),
    (0, swagger_1.ApiQuery)({ name: "pageSize", required: false }),
    (0, swagger_1.ApiQuery)({ name: "pageIndex", required: false }),
    (0, common_1.Get)(),
    __param(0, (0, common_1.Query)("id")),
    __param(1, (0, common_1.Query)("search")),
    __param(2, (0, common_1.Query)("pageSize")),
    __param(3, (0, common_1.Query)("pageIndex")),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, String, String, String]),
    __metadata("design:returntype", Promise)
], LocationController.prototype, "get", null);
__decorate([
    (0, role_decorator_1.Roles)(user_enum_1.UserRole.ADMIN),
    (0, swagger_1.ApiConsumes)("multipart/form-data"),
    (0, swagger_1.ApiBody)({
        type: location_entities_1.Location,
    }),
    (0, common_1.UseInterceptors)((0, platform_express_1.FileInterceptor)("photo")),
    (0, common_1.Post)("new-location"),
    __param(0, (0, common_1.Body)(check_existence_pipe_1.CheckExistencePipe)),
    __param(1, (0, common_1.UploadedFile)(compress_image_pipe_1.CompressImagePipe)),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [create_location_dto_1.CreateLocationDto, Object]),
    __metadata("design:returntype", Promise)
], LocationController.prototype, "create", null);
__decorate([
    (0, role_decorator_1.Roles)(user_enum_1.UserRole.ADMIN),
    (0, swagger_1.ApiConsumes)("multipart/form-data"),
    (0, swagger_1.ApiBody)({
        type: update_location_dto_1.UpdateLocationDto,
    }),
    (0, common_1.UseInterceptors)((0, platform_express_1.FileInterceptor)("photo")),
    (0, common_1.Put)("update/:id"),
    __param(0, (0, common_1.Param)("id")),
    __param(1, (0, common_1.Body)()),
    __param(2, (0, common_1.UploadedFile)(compress_image_pipe_1.CompressImagePipe)),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, update_location_dto_1.UpdateLocationDto, Object]),
    __metadata("design:returntype", Promise)
], LocationController.prototype, "update", null);
__decorate([
    (0, role_decorator_1.Roles)(user_enum_1.UserRole.ADMIN),
    (0, common_1.Delete)("delete/:id"),
    __param(0, (0, common_1.Param)("id")),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], LocationController.prototype, "delete", null);
exports.LocationController = LocationController = __decorate([
    (0, common_1.UseGuards)(local_auth_guard_1.LocalAuthGuard),
    (0, swagger_1.ApiTags)("Location"),
    (0, common_1.Controller)("location"),
    __metadata("design:paramtypes", [location_service_1.LocationService])
], LocationController);
//# sourceMappingURL=location.controller.js.map