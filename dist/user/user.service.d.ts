import { PrismaService } from "../prisma/prisma.service";
import { User } from "./entities/user.entity";
export declare class UserService {
    private prisma;
    constructor(prisma: PrismaService);
    findAll(searchKeyword: string, pageSize: number, pageIndex: number): Promise<User[]>;
    findOne(id: number): Promise<User>;
    isExisted({ email, phone }: User): Promise<User>;
    create(data: any): Promise<User>;
    update(id: number, data: any): Promise<User>;
    remove(id: number): Promise<User | Error>;
}
