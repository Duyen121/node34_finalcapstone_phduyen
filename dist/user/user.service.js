"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserService = void 0;
const common_1 = require("@nestjs/common");
const prisma_service_1 = require("../prisma/prisma.service");
const bcrypt = require("bcrypt");
let UserService = class UserService {
    constructor(prisma) {
        this.prisma = prisma;
    }
    async findAll(searchKeyword, pageSize, pageIndex) {
        let data = await this.prisma.users.findMany({
            where: searchKeyword?.trim() && {
                OR: [
                    {
                        name: {
                            contains: searchKeyword,
                        },
                    },
                    {
                        email: {
                            contains: searchKeyword,
                        },
                    },
                    {
                        phone: {
                            contains: searchKeyword,
                        },
                    },
                ],
            },
            skip: pageSize * (pageIndex - 1) || undefined,
            take: pageSize || undefined,
        });
        return data;
    }
    async findOne(id) {
        let user = await this.prisma.users.findUnique({ where: { id } });
        return user;
    }
    async isExisted({ email, phone }) {
        let user = await this.prisma.users.findFirst({
            where: {
                OR: [{ email }, { phone }],
            },
        });
        return user;
    }
    async create(data) {
        let hashedPassword = await bcrypt.hash(data.pass_word, 10);
        let user = await this.prisma.users.create({
            data: { ...data, pass_word: hashedPassword },
        });
        return user;
    }
    async update(id, data) {
        let user = await this.prisma.users.findUnique({
            where: { id },
        });
        if (!user) {
            throw new common_1.HttpException("Not existed user!", 422);
        }
        let updatedUser = await this.prisma.users.update({
            where: { id },
            data,
        });
        return updatedUser;
    }
    async remove(id) {
        let isExisted = await this.findOne(id);
        if (isExisted) {
            let user = await this.prisma.users.delete({ where: { id } });
            return user;
        }
        else
            throw new common_1.NotFoundException();
    }
};
exports.UserService = UserService;
exports.UserService = UserService = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [prisma_service_1.PrismaService])
], UserService);
//# sourceMappingURL=user.service.js.map