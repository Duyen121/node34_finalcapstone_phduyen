export declare enum UserRole {
    USER = "user",
    ADMIN = "admin"
}
export declare enum UserGender {
    MALE = "male",
    FEMALE = "female",
    OTHER = "other"
}
