"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserModule = void 0;
const common_1 = require("@nestjs/common");
const user_service_1 = require("./user.service");
const user_controller_1 = require("./user.controller");
const local_strategy_1 = require("../auth/strategies/local.strategy");
const core_1 = require("@nestjs/core");
const user_role_guard_1 = require("../auth/guards/user-role.guard");
const jwt_1 = require("@nestjs/jwt");
const platform_express_1 = require("@nestjs/platform-express");
const multer_1 = require("multer");
let UserModule = class UserModule {
};
exports.UserModule = UserModule;
exports.UserModule = UserModule = __decorate([
    (0, common_1.Module)({
        imports: [
            platform_express_1.MulterModule.register({
                storage: (0, multer_1.diskStorage)({
                    destination: process.cwd() + "/public/img/user-avatar",
                    filename: (req, file, callback) => callback(null, new Date().getTime() + "_" + file.originalname),
                }),
            }),
        ],
        controllers: [user_controller_1.UserController],
        providers: [user_service_1.UserService, local_strategy_1.LocalStrategy, {
                provide: core_1.APP_GUARD,
                useClass: user_role_guard_1.UserRolesGuard,
            }, jwt_1.JwtService]
    })
], UserModule);
//# sourceMappingURL=user.module.js.map