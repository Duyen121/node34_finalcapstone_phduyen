/// <reference types="multer" />
import { UserService } from "./user.service";
import { UpdateUserDto } from "./dto/update-user.dto";
import { CreateUserDto } from "./dto/create-user.dto";
export declare class UserController {
    private readonly userService;
    constructor(userService: UserService);
    get(id: string, searchKeyword: string, pageSize: string, pageIndex: string): Promise<{
        message: string;
        data: () => Record<string, any>;
    }>;
    create(data: CreateUserDto): Promise<{
        message: string;
        data: () => Record<string, any>;
    }>;
    update(id: string, data: UpdateUserDto): Promise<{
        message: string;
        data: () => Record<string, any>;
    }>;
    uploadAvatar(req: any, file: Express.Multer.File): Promise<{
        message: string;
    }>;
    delete(id: number): Promise<{
        message: string;
    }>;
}
