import { UserGender, UserRole } from '../user.enum';
export declare class CreateUserDto {
    avatar?: string;
    name: string;
    email: string;
    pass_word: string;
    phone: string;
    birth_day: Date;
    gender: UserGender;
    role: UserRole;
}
