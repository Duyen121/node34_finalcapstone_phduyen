import { CreateUserDto } from "./create-user.dto";
import { UserGender, UserRole } from "../user.enum";
declare const UpdateUserDto_base: import("@nestjs/mapped-types").MappedType<Partial<CreateUserDto>>;
export declare class UpdateUserDto extends UpdateUserDto_base {
    name: string;
    email: string;
    phone: string;
    birth_day: Date;
    gender: UserGender;
    role: UserRole;
}
export {};
