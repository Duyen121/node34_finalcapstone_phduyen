export declare class User {
    id?: number;
    avatar?: string;
    name: string;
    email: string;
    pass_word: string;
    phone: string;
    birth_day: Date;
    gender: 'male' | 'female' | 'other';
    role?: 'user' | 'admin';
}
