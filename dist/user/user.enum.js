"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserGender = exports.UserRole = void 0;
var UserRole;
(function (UserRole) {
    UserRole["USER"] = "user";
    UserRole["ADMIN"] = "admin";
})(UserRole || (exports.UserRole = UserRole = {}));
var UserGender;
(function (UserGender) {
    UserGender["MALE"] = "male";
    UserGender["FEMALE"] = "female";
    UserGender["OTHER"] = "other";
})(UserGender || (exports.UserGender = UserGender = {}));
//# sourceMappingURL=user.enum.js.map