"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserController = void 0;
const common_1 = require("@nestjs/common");
const user_service_1 = require("./user.service");
const swagger_1 = require("@nestjs/swagger");
const class_transformer_1 = require("class-transformer");
const user_entity_1 = require("./entities/user.entity");
const update_user_dto_1 = require("./dto/update-user.dto");
const platform_express_1 = require("@nestjs/platform-express");
const check_existence_pipe_1 = require("../pipes/check-existence.pipe");
const compress_image_pipe_1 = require("../pipes/compress-image.pipe");
const create_user_dto_1 = require("./dto/create-user.dto");
const upload_avatar_dto_1 = require("./dto/upload-avatar.dto");
const local_auth_guard_1 = require("../auth/guards/local-auth.guard");
const role_decorator_1 = require("../auth/decorators/role.decorator");
const user_enum_1 = require("./user.enum");
let UserController = class UserController {
    constructor(userService) {
        this.userService = userService;
    }
    async get(id, searchKeyword, pageSize, pageIndex) {
        try {
            let data;
            if (+id) {
                data = await this.userService.findOne(+id);
            }
            else {
                data = await this.userService.findAll(searchKeyword, +pageSize, +pageIndex);
            }
            if (data?.length === 0 || !data) {
                throw new common_1.HttpException("No result found", 200);
            }
            else {
                return {
                    message: "Successfully!",
                    data: () => (0, class_transformer_1.instanceToPlain)((0, class_transformer_1.plainToClass)(user_entity_1.User, data)),
                };
            }
        }
        catch (err) {
            throw err || new common_1.InternalServerErrorException();
        }
    }
    async create(data) {
        try {
            let user = await this.userService.create({
                ...data,
                avatar: "",
            });
            return {
                message: "Successfully created!",
                data: () => (0, class_transformer_1.instanceToPlain)((0, class_transformer_1.plainToClass)(user_entity_1.User, user)),
            };
        }
        catch (err) {
            throw err || new common_1.InternalServerErrorException();
        }
    }
    async update(id, data) {
        try {
            let updatedUser = await this.userService.update(+id, {
                ...data,
            });
            return {
                message: "Successfully updated!",
                data: () => (0, class_transformer_1.instanceToPlain)((0, class_transformer_1.plainToClass)(user_entity_1.User, updatedUser))
            };
        }
        catch (err) {
            throw err || new common_1.InternalServerErrorException();
        }
    }
    async uploadAvatar(req, file) {
        let data = req.user.data;
        let userId = data.id;
        if (!file) {
            throw new common_1.HttpException("Please upload your avatar!", 422);
        }
        await this.userService.update(+userId, {
            ...data,
            avatar: data?.avatar !== null ? file && file?.filename : null,
        });
        return { message: "Successfully uploaded avatar!" };
    }
    async delete(id) {
        try {
            await this.userService.remove(+id);
            return { message: "Successfully deleted!" };
        }
        catch (err) {
            throw err || new common_1.InternalServerErrorException();
        }
    }
};
exports.UserController = UserController;
__decorate([
    (0, swagger_1.ApiQuery)({ name: "id", required: false }),
    (0, swagger_1.ApiQuery)({ name: "search", required: false }),
    (0, swagger_1.ApiQuery)({ name: "pageSize", required: false }),
    (0, swagger_1.ApiQuery)({ name: "pageIndex", required: false }),
    (0, common_1.Get)(),
    __param(0, (0, common_1.Query)("id")),
    __param(1, (0, common_1.Query)("search")),
    __param(2, (0, common_1.Query)("pageSize")),
    __param(3, (0, common_1.Query)("pageIndex")),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, String, String, String]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "get", null);
__decorate([
    (0, role_decorator_1.Roles)(user_enum_1.UserRole.ADMIN),
    (0, swagger_1.ApiConsumes)("multipart/form-data"),
    (0, swagger_1.ApiBody)({
        type: user_entity_1.User,
    }),
    (0, common_1.Post)("new-user"),
    __param(0, (0, common_1.Body)(check_existence_pipe_1.CheckExistencePipe)),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [create_user_dto_1.CreateUserDto]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "create", null);
__decorate([
    (0, swagger_1.ApiConsumes)("multipart/form-data"),
    (0, swagger_1.ApiBody)({
        type: update_user_dto_1.UpdateUserDto,
    }),
    (0, common_1.Put)("update/:id"),
    __param(0, (0, common_1.Param)("id")),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, update_user_dto_1.UpdateUserDto]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "update", null);
__decorate([
    (0, swagger_1.ApiConsumes)("multipart/form-data"),
    (0, swagger_1.ApiBody)({
        type: upload_avatar_dto_1.UploadAvatarDto,
    }),
    (0, common_1.UseInterceptors)((0, platform_express_1.FileInterceptor)("avatar")),
    (0, common_1.Post)("upload-avatar"),
    __param(0, (0, common_1.Req)()),
    __param(1, (0, common_1.UploadedFile)(compress_image_pipe_1.CompressImagePipe)),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "uploadAvatar", null);
__decorate([
    (0, role_decorator_1.Roles)(user_enum_1.UserRole.ADMIN),
    (0, common_1.Delete)("delete/:id"),
    __param(0, (0, common_1.Param)("id")),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "delete", null);
exports.UserController = UserController = __decorate([
    (0, common_1.UseGuards)(local_auth_guard_1.LocalAuthGuard),
    (0, swagger_1.ApiTags)("User"),
    (0, common_1.Controller)("user"),
    __metadata("design:paramtypes", [user_service_1.UserService])
], UserController);
//# sourceMappingURL=user.controller.js.map