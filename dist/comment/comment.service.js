"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CommentService = void 0;
const common_1 = require("@nestjs/common");
const prisma_service_1 = require("../prisma/prisma.service");
let CommentService = class CommentService {
    constructor(prisma) {
        this.prisma = prisma;
    }
    async findAll(roomId, pageSize, pageIndex) {
        let data = await this.prisma.comments.findMany({
            where: { room_id: roomId || undefined },
            include: {
                users: {
                    select: {
                        name: true,
                    },
                },
                rooms: {
                    select: {
                        room_name: true,
                    },
                },
            },
            skip: pageSize * (pageIndex - 1) || undefined,
            take: pageSize || undefined,
        });
        return data;
    }
    async findOne(id) {
        let comment = await this.prisma.comments.findFirst({
            where: { id },
        });
        return comment;
    }
    async create(data, user) {
        let comment = await this.prisma.comments.create({
            data: { ...data, user_id: user.data.id },
            include: {
                rooms: {
                    select: {
                        room_name: true,
                    },
                },
            },
        });
        return comment;
    }
    async update(id, data) {
        let comment = await this.prisma.comments.update({
            where: { id },
            data,
            include: {
                rooms: {
                    select: {
                        room_name: true,
                    },
                },
            },
        });
        return comment;
    }
    async remove(id) {
        let comment = await this.prisma.comments.delete({
            where: { id },
        });
        return comment;
    }
};
exports.CommentService = CommentService;
exports.CommentService = CommentService = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [prisma_service_1.PrismaService])
], CommentService);
//# sourceMappingURL=comment.service.js.map