"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CommentController = void 0;
const common_1 = require("@nestjs/common");
const jwt_1 = require("@nestjs/jwt");
const comment_service_1 = require("./comment.service");
const swagger_1 = require("@nestjs/swagger");
const class_transformer_1 = require("class-transformer");
const create_comment_dto_1 = require("./dto/create-comment.dto");
const update_comment_dto_1 = require("./dto/update-comment.dto");
const user_comment_entity_1 = require("./entities/user-comment.entity");
const local_auth_guard_1 = require("../auth/guards/local-auth.guard");
let CommentController = class CommentController {
    constructor(commentService, jwtService) {
        this.commentService = commentService;
        this.jwtService = jwtService;
    }
    async get(id, roomID, pageSize, pageIndex) {
        try {
            let data;
            if (id) {
                data = await this.commentService.findOne(+id);
            }
            else {
                data = await this.commentService.findAll(+roomID, +pageSize, +pageIndex);
            }
            if (data?.length === 0 || !data) {
                throw new common_1.HttpException("No result found", 200);
            }
            else {
                return {
                    message: "Successfully!",
                    data: (0, class_transformer_1.instanceToPlain)((0, class_transformer_1.plainToClass)(user_comment_entity_1.UserComment, data)),
                };
            }
        }
        catch (err) {
            throw err || new common_1.InternalServerErrorException();
        }
    }
    async create(data, req) {
        try {
            const authorization = req.headers["authorization"];
            const [_, token] = authorization.split(" ");
            let user = this.jwtService.decode(token);
            let comment = await this.commentService.create(data, user);
            return { message: "Successfully posted!", data: (0, class_transformer_1.instanceToPlain)((0, class_transformer_1.plainToClass)(user_comment_entity_1.UserComment, comment)), };
        }
        catch (err) {
            throw err || new common_1.InternalServerErrorException();
        }
    }
    async udpate(id, data) {
        try {
            let comment = await this.commentService.update(+id, data);
            return { message: "Successfully updated!", data: (0, class_transformer_1.instanceToPlain)((0, class_transformer_1.plainToClass)(user_comment_entity_1.UserComment, comment)), };
        }
        catch (err) {
            throw err || new common_1.InternalServerErrorException();
        }
    }
    async delete(id) {
        try {
            await this.commentService.remove(+id);
            return { message: "Successfully deleted!" };
        }
        catch (err) {
            throw err || new common_1.InternalServerErrorException();
        }
    }
};
exports.CommentController = CommentController;
__decorate([
    (0, swagger_1.ApiQuery)({ name: "id", required: false }),
    (0, swagger_1.ApiQuery)({ name: "roomId", required: false }),
    (0, swagger_1.ApiQuery)({ name: "pageSize", required: false }),
    (0, swagger_1.ApiQuery)({ name: "pageIndex", required: false }),
    (0, common_1.Get)(""),
    __param(0, (0, common_1.Query)("id")),
    __param(1, (0, common_1.Query)("roomId")),
    __param(2, (0, common_1.Query)("pageSize")),
    __param(3, (0, common_1.Query)("pageIndex")),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, String, String, String]),
    __metadata("design:returntype", Promise)
], CommentController.prototype, "get", null);
__decorate([
    (0, common_1.Post)("new-comment"),
    __param(0, (0, common_1.Body)()),
    __param(1, (0, common_1.Req)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [create_comment_dto_1.CreateCommentDto, Request]),
    __metadata("design:returntype", Promise)
], CommentController.prototype, "create", null);
__decorate([
    (0, common_1.Put)("update/:id"),
    __param(0, (0, common_1.Param)("id")),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, update_comment_dto_1.UpdateCommentDto]),
    __metadata("design:returntype", Promise)
], CommentController.prototype, "udpate", null);
__decorate([
    (0, common_1.Delete)("delete/:id"),
    __param(0, (0, common_1.Param)("id")),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], CommentController.prototype, "delete", null);
exports.CommentController = CommentController = __decorate([
    (0, common_1.UseGuards)(local_auth_guard_1.LocalAuthGuard),
    (0, swagger_1.ApiTags)("Comments"),
    (0, common_1.Controller)("comment"),
    __metadata("design:paramtypes", [comment_service_1.CommentService,
        jwt_1.JwtService])
], CommentController);
//# sourceMappingURL=comment.controller.js.map