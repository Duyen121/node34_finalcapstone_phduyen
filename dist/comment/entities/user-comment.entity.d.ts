import { User } from 'src/user/entities/user.entity';
export declare class UserComment {
    id?: number;
    room_id: number;
    user_id: number;
    user: User;
    content: string;
    rated: number;
}
