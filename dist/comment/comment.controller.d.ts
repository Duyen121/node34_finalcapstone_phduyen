import { JwtService } from "@nestjs/jwt";
import { CommentService } from "./comment.service";
import { CreateCommentDto } from "./dto/create-comment.dto";
import { UpdateCommentDto } from "./dto/update-comment.dto";
export declare class CommentController {
    private readonly commentService;
    private jwtService;
    constructor(commentService: CommentService, jwtService: JwtService);
    get(id: string, roomID: string, pageSize: string, pageIndex: string): Promise<{
        message: string;
        data: Record<string, any>;
    }>;
    create(data: CreateCommentDto, req: Request): Promise<{
        message: string;
        data: Record<string, any>;
    }>;
    udpate(id: string, data: UpdateCommentDto): Promise<{
        message: string;
        data: Record<string, any>;
    }>;
    delete(id: string): Promise<{
        message: string;
    }>;
}
