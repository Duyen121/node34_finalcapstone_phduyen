export declare class CreateCommentDto {
    id?: number;
    room_id: number;
    user_id: number;
    content: string;
    rated: number;
}
