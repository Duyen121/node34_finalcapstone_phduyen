export declare class UpdateCommentDto {
    room_id: number;
    user_id: number;
    content: string;
    rated: number;
}
