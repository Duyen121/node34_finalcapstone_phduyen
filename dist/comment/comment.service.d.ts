import { PrismaService } from "src/prisma/prisma.service";
import { CreateCommentDto } from "./dto/create-comment.dto";
import { UpdateCommentDto } from "./dto/update-comment.dto";
import { UserComment } from "./entities/user-comment.entity";
export declare class CommentService {
    private prisma;
    constructor(prisma: PrismaService);
    findAll(roomId: number, pageSize: number, pageIndex: number): Promise<UserComment[]>;
    findOne(id: number): Promise<UserComment>;
    create(data: CreateCommentDto, user: any): Promise<CreateCommentDto>;
    update(id: number, data: UpdateCommentDto): Promise<UpdateCommentDto>;
    remove(id: number): Promise<UserComment>;
}
