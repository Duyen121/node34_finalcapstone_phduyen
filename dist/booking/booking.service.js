"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BookingService = void 0;
const common_1 = require("@nestjs/common");
const prisma_service_1 = require("../prisma/prisma.service");
let BookingService = class BookingService {
    constructor(prisma) {
        this.prisma = prisma;
    }
    async findAll(userId, searchKeyword, pageSize, pageIndex) {
        let data = await this.prisma.room_bookings.findMany({
            where: {
                OR: searchKeyword?.trim()
                    ? [
                        {
                            users: {
                                OR: [
                                    { name: { contains: searchKeyword } },
                                    { email: { contains: searchKeyword } },
                                    { phone: { contains: searchKeyword } },
                                ],
                            },
                        },
                    ]
                    : undefined,
                user_id: userId || undefined,
            },
            include: {
                users: {
                    select: {
                        name: true,
                    },
                },
            },
            skip: pageSize * (pageIndex - 1) || undefined,
            take: pageSize || undefined,
        });
        return data;
    }
    async findOne(id) {
        let booking = await this.prisma.room_bookings.findFirst({
            where: { id },
        });
        return booking;
    }
    async create(data, user) {
        let booking = await this.prisma.room_bookings.create({
            data: { ...data, user_id: user.data.id },
            include: {
                rooms: {
                    select: {
                        room_name: true,
                        locations: {
                            select: {
                                address: true,
                                city: true,
                                country: true,
                            },
                        },
                    },
                },
            },
        });
        return booking;
    }
    async update(id, data) {
        let booking = await this.prisma.room_bookings.update({
            where: { id },
            data,
            include: {
                rooms: {
                    select: {
                        room_name: true,
                        locations: {
                            select: {
                                address: true,
                                city: true,
                                country: true
                            }
                        }
                    },
                },
            },
        });
        return booking;
    }
    async remove(id) {
        let booking = await this.prisma.room_bookings.delete({
            where: { id },
        });
        return booking;
    }
};
exports.BookingService = BookingService;
exports.BookingService = BookingService = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [prisma_service_1.PrismaService])
], BookingService);
//# sourceMappingURL=booking.service.js.map