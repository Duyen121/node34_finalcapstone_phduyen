import { BookingService } from "./booking.service";
import { CreateBookingDto } from "./dto/create-booking.dto";
import { UpdateBookingDto } from "./dto/update-booking.dto";
import { JwtService } from "@nestjs/jwt";
import { RoomService } from "src/room/room.service";
export declare class BookingController {
    private readonly bookingService;
    private readonly roomService;
    private jwtService;
    constructor(bookingService: BookingService, roomService: RoomService, jwtService: JwtService);
    get(id: string, userId: string, searchKeyword: string, pageSize: string, pageIndex: string): Promise<{
        message: string;
        data: Record<string, any>;
    }>;
    create(data: CreateBookingDto, req: Request): Promise<{
        message: string;
        data: Record<string, any>;
    }>;
    update(id: string, data: UpdateBookingDto): Promise<{
        message: string;
        data: Record<string, any>;
    }>;
    delete(id: string): Promise<{
        message: string;
    }>;
}
