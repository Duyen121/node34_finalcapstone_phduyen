import { User } from 'src/user/entities/user.entity';
export declare class Booking {
    id?: number;
    user_id: number;
    user: User;
    room_id: number;
    customer_number: number;
    arrival_date: Date;
    departure_date: Date;
}
