import { PrismaService } from "src/prisma/prisma.service";
import { Booking } from "./entities/booking.entity";
import { CreateBookingDto } from "./dto/create-booking.dto";
import { UpdateBookingDto } from "./dto/update-booking.dto";
export declare class BookingService {
    private prisma;
    constructor(prisma: PrismaService);
    findAll(userId: number, searchKeyword: string, pageSize: number, pageIndex: number): Promise<Booking[]>;
    findOne(id: number): Promise<Booking>;
    create(data: CreateBookingDto, user: any): Promise<CreateBookingDto>;
    update(id: number, data: UpdateBookingDto): Promise<UpdateBookingDto>;
    remove(id: number): Promise<Booking>;
}
