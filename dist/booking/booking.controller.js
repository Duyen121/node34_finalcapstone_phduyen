"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BookingController = void 0;
const common_1 = require("@nestjs/common");
const booking_service_1 = require("./booking.service");
const swagger_1 = require("@nestjs/swagger");
const class_transformer_1 = require("class-transformer");
const booking_entity_1 = require("./entities/booking.entity");
const create_booking_dto_1 = require("./dto/create-booking.dto");
const update_booking_dto_1 = require("./dto/update-booking.dto");
const local_auth_guard_1 = require("../auth/guards/local-auth.guard");
const jwt_1 = require("@nestjs/jwt");
const room_service_1 = require("../room/room.service");
let BookingController = class BookingController {
    constructor(bookingService, roomService, jwtService) {
        this.bookingService = bookingService;
        this.roomService = roomService;
        this.jwtService = jwtService;
    }
    async get(id, userId, searchKeyword, pageSize, pageIndex) {
        try {
            let data;
            if (id) {
                data = await this.bookingService.findOne(+id);
            }
            else {
                data = await this.bookingService.findAll(+userId, searchKeyword, +pageSize, +pageIndex);
            }
            if (data?.length === 0 || !data) {
                throw new common_1.HttpException("No result found", 200);
            }
            else {
                return {
                    message: "Successfully!",
                    data: (0, class_transformer_1.instanceToPlain)((0, class_transformer_1.plainToClass)(booking_entity_1.Booking, data)),
                };
            }
        }
        catch (err) {
            throw err || new common_1.InternalServerErrorException();
        }
    }
    async create(data, req) {
        try {
            const authorization = req.headers["authorization"];
            const [_, token] = authorization.split(" ");
            let user = this.jwtService.decode(token);
            let room = await this.roomService.findOne(+data.room_id);
            if (!(data.customer_number <= room.customer_per_room)) {
                throw new common_1.HttpException("Invalid customer number", 422);
            }
            let booking = await this.bookingService.create(data, user);
            return {
                message: "A new reservation is successfully created!",
                data: (0, class_transformer_1.instanceToPlain)((0, class_transformer_1.plainToClass)(booking_entity_1.Booking, booking)),
            };
        }
        catch (err) {
            throw err || new common_1.InternalServerErrorException();
        }
    }
    async update(id, data) {
        try {
            let room = await this.roomService.findOne(+data.room_id);
            if (!(data.customer_number <= room.customer_per_room)) {
                throw new common_1.HttpException("Invalid customer number", 422);
            }
            let booking = await this.bookingService.update(+id, data);
            return { message: "Successfully updated!", data: (0, class_transformer_1.instanceToPlain)((0, class_transformer_1.plainToClass)(booking_entity_1.Booking, booking)), };
        }
        catch (err) {
            throw err || new common_1.InternalServerErrorException();
        }
    }
    async delete(id) {
        try {
            await this.bookingService.remove(+id);
            return { message: "Successfully deleted!" };
        }
        catch (err) {
            throw err || new common_1.InternalServerErrorException();
        }
    }
};
exports.BookingController = BookingController;
__decorate([
    (0, swagger_1.ApiQuery)({ name: "userId", required: false }),
    (0, swagger_1.ApiQuery)({ name: "id", required: false }),
    (0, swagger_1.ApiQuery)({ name: "search", required: false }),
    (0, swagger_1.ApiQuery)({ name: "pageSize", required: false }),
    (0, swagger_1.ApiQuery)({ name: "pageIndex", required: false }),
    (0, common_1.Get)(),
    __param(0, (0, common_1.Query)("id")),
    __param(1, (0, common_1.Query)("userId")),
    __param(2, (0, common_1.Query)("search")),
    __param(3, (0, common_1.Query)("pageSize")),
    __param(4, (0, common_1.Query)("pageIndex")),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, String, String, String, String]),
    __metadata("design:returntype", Promise)
], BookingController.prototype, "get", null);
__decorate([
    (0, common_1.Post)("new-booking"),
    __param(0, (0, common_1.Body)()),
    __param(1, (0, common_1.Req)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [create_booking_dto_1.CreateBookingDto, Request]),
    __metadata("design:returntype", Promise)
], BookingController.prototype, "create", null);
__decorate([
    (0, common_1.Put)("update/:id"),
    __param(0, (0, common_1.Param)("id")),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, update_booking_dto_1.UpdateBookingDto]),
    __metadata("design:returntype", Promise)
], BookingController.prototype, "update", null);
__decorate([
    (0, common_1.Delete)("delete/:id"),
    __param(0, (0, common_1.Param)("id")),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], BookingController.prototype, "delete", null);
exports.BookingController = BookingController = __decorate([
    (0, common_1.UseGuards)(local_auth_guard_1.LocalAuthGuard),
    (0, swagger_1.ApiTags)("Booking"),
    (0, common_1.Controller)("booking"),
    __metadata("design:paramtypes", [booking_service_1.BookingService,
        room_service_1.RoomService,
        jwt_1.JwtService])
], BookingController);
//# sourceMappingURL=booking.controller.js.map