export declare class UpdateBookingDto {
    user_id: number;
    room_id: number;
    customer_number: number;
    arrival_date: Date;
    departure_date: Date;
}
