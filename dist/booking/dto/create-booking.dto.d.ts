export declare class CreateBookingDto {
    id?: number;
    user_id: number;
    room_id: number;
    customer_number: number;
    arrival_date: Date;
    departure_date: Date;
}
