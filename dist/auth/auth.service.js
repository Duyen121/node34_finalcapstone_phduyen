"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthService = void 0;
const common_1 = require("@nestjs/common");
const config_1 = require("@nestjs/config");
const jwt_1 = require("@nestjs/jwt");
const prisma_service_1 = require("../prisma/prisma.service");
const user_service_1 = require("../user/user.service");
const bcrypt = require("bcrypt");
let AuthService = class AuthService {
    constructor(prisma, jwtService, userService, configService) {
        this.prisma = prisma;
        this.jwtService = jwtService;
        this.userService = userService;
        this.configService = configService;
    }
    async signIn(body) {
        let { email, pass_word } = body;
        let user = await this.prisma.users.findFirst({
            where: { email },
        });
        if (!user) {
            throw new common_1.ForbiddenException("Credentials incorrect");
        }
        let pwMatches = await bcrypt.compare(pass_word, user.pass_word);
        if (!pwMatches) {
            throw new common_1.ForbiddenException("Credentials incorrect");
        }
        let data = this.jwtService.sign({ data: user }, { expiresIn: "1h", secret: this.configService.get("SECRET_KEY") });
        return { message: "Successfully signed in!", accessToken: data };
    }
    async signUp(body) {
        let user = await this.userService.create({
            ...body,
            role: "user",
            avatar: ""
        });
        return {
            message: "Sign up successfully!",
        };
    }
};
exports.AuthService = AuthService;
exports.AuthService = AuthService = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [prisma_service_1.PrismaService,
        jwt_1.JwtService,
        user_service_1.UserService,
        config_1.ConfigService])
], AuthService);
//# sourceMappingURL=auth.service.js.map