import { AuthService } from "./auth.service";
import { SignInDto } from "./dto/sign-in.dto";
import { SignUpDto } from "./dto/sign-up.dto";
export declare class AuthController {
    private authService;
    constructor(authService: AuthService);
    signIn(body: SignInDto): Promise<any>;
    signUp(body: SignUpDto): Promise<{
        message: string;
    }>;
}
