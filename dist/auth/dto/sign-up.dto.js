"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SignUpDto = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_validator_1 = require("class-validator");
const user_enum_1 = require("../../user/user.enum");
class SignUpDto {
}
exports.SignUpDto = SignUpDto;
__decorate([
    (0, swagger_1.ApiPropertyOptional)({ type: 'string', format: 'binary' }),
    __metadata("design:type", String)
], SignUpDto.prototype, "avatar", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ default: 'name' }),
    (0, class_validator_1.Matches)(/\D+/g, { message: 'Invalid name!' }),
    (0, class_validator_1.IsNotEmpty)(),
    __metadata("design:type", String)
], SignUpDto.prototype, "name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ default: 'username123@gmail.com' }),
    (0, class_validator_1.IsEmail)(undefined, { message: 'Invalid email!' }),
    (0, class_validator_1.IsNotEmpty)(),
    __metadata("design:type", String)
], SignUpDto.prototype, "email", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, class_validator_1.IsNotEmpty)(),
    __metadata("design:type", String)
], SignUpDto.prototype, "pass_word", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ default: '0912345678' }),
    (0, class_validator_1.IsNumberString)(undefined, { message: 'Invalid phone number' }),
    (0, class_validator_1.IsNotEmpty)(),
    __metadata("design:type", String)
], SignUpDto.prototype, "phone", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ default: new Date().toISOString() }),
    (0, class_validator_1.IsDateString)(undefined, { message: 'Invalid date' }),
    (0, class_validator_1.IsNotEmpty)(),
    __metadata("design:type", Date)
], SignUpDto.prototype, "birth_day", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, class_validator_1.IsNotEmpty)(),
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsEnum)(user_enum_1.UserGender),
    __metadata("design:type", String)
], SignUpDto.prototype, "gender", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ enum: ['user', 'admin'] }),
    (0, class_validator_1.IsNotEmpty)(),
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsEnum)(user_enum_1.UserRole),
    __metadata("design:type", String)
], SignUpDto.prototype, "role", void 0);
//# sourceMappingURL=sign-up.dto.js.map