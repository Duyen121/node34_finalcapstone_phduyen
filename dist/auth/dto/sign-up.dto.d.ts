import { UserGender, UserRole } from 'src/user/user.enum';
export declare class SignUpDto {
    avatar?: string;
    name: string;
    email: string;
    pass_word: string;
    phone: string;
    birth_day: Date;
    gender: UserGender;
    role: UserRole;
}
