import { CanActivate, ExecutionContext } from "@nestjs/common";
import { Reflector } from "@nestjs/core";
import { UserRole } from "src/user/user.enum";
import { JwtService } from "@nestjs/jwt";
export declare class UserRolesGuard implements CanActivate {
    private reflector;
    private jwtService;
    constructor(reflector: Reflector, jwtService: JwtService);
    matchRoles(roles: string[], userRole: UserRole): Promise<boolean>;
    canActivate(context: ExecutionContext): boolean | Promise<boolean>;
}
