import { Strategy } from 'passport-jwt';
import { UserService } from '../../user/user.service';
declare const LocalStrategy_base: new (...args: any[]) => Strategy;
export declare class LocalStrategy extends LocalStrategy_base {
    private readonly userService;
    constructor(userService: UserService);
    validate(decodedToken: any): Promise<any>;
}
export {};
