import { ConfigService } from "@nestjs/config";
import { JwtService } from "@nestjs/jwt";
import { PrismaService } from "src/prisma/prisma.service";
import { UserService } from "src/user/user.service";
import { SignInDto } from "./dto/sign-in.dto";
import { SignUpDto } from "./dto/sign-up.dto";
export declare class AuthService {
    private prisma;
    private jwtService;
    private userService;
    private configService;
    constructor(prisma: PrismaService, jwtService: JwtService, userService: UserService, configService: ConfigService);
    signIn(body: SignInDto): Promise<any>;
    signUp(body: SignUpDto): Promise<{
        message: string;
    }>;
}
