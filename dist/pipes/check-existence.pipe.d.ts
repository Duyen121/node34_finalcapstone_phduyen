import { PipeTransform, ArgumentMetadata } from '@nestjs/common';
import { User } from 'src/user/entities/user.entity';
import { UserService } from 'src/user/user.service';
export declare class CheckExistencePipe implements PipeTransform {
    private readonly usersService;
    constructor(usersService: UserService);
    transform(user: User, metadata: ArgumentMetadata): Promise<User>;
}
