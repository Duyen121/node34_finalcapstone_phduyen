"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const core_1 = require("@nestjs/core");
const app_module_1 = require("./app.module");
const express = require("express");
const common_1 = require("@nestjs/common");
const { DOMAIN, PORT } = process.env;
async function bootstrap() {
    const app = await core_1.NestFactory.create(app_module_1.AppModule);
    app.enableCors();
    app.setGlobalPrefix("api");
    app.use(express.static("public"));
    app.useGlobalPipes(new common_1.ValidationPipe({ skipMissingProperties: true }));
    app.useGlobalInterceptors(new common_1.ClassSerializerInterceptor(app.get(core_1.Reflector)));
    await app.listen(PORT, () => {
        console.log(`Server is starting at ${DOMAIN}:${PORT}...`);
    });
}
bootstrap();
//# sourceMappingURL=main.js.map