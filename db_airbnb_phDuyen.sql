-- -------------------------------------------------------------
-- TablePlus 5.6.8(524)
--
-- https://tableplus.com/
--
-- Database: db_airbnb
-- Generation Time: 2023-12-24 15:37:31.8880
-- -------------------------------------------------------------


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


CREATE TABLE `comments` (
  `id` int NOT NULL AUTO_INCREMENT,
  `room_id` int NOT NULL,
  `user_id` int NOT NULL,
  `content` varchar(255) NOT NULL,
  `rated` int NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `room_id` (`room_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `comments_ibfk_1` FOREIGN KEY (`room_id`) REFERENCES `rooms` (`id`),
  CONSTRAINT `comments_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `locations` (
  `id` int NOT NULL AUTO_INCREMENT,
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `city` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `photo` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `room_bookings` (
  `id` int NOT NULL AUTO_INCREMENT,
  `room_id` int NOT NULL,
  `arrival_date` datetime NOT NULL,
  `departure_date` datetime NOT NULL,
  `customer_number` int NOT NULL,
  `user_id` int NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `room_id` (`room_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `room_bookings_ibfk_1` FOREIGN KEY (`room_id`) REFERENCES `rooms` (`id`),
  CONSTRAINT `room_bookings_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `rooms` (
  `id` int NOT NULL AUTO_INCREMENT,
  `room_name` varchar(255) NOT NULL,
  `customer_per_room` int NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `image` varchar(255) NOT NULL,
  `location_id` int NOT NULL,
  `has_ironing_board` tinyint(1) NOT NULL,
  `has_pool` tinyint(1) NOT NULL,
  `has_parking` tinyint(1) NOT NULL,
  `has_stove` tinyint(1) NOT NULL,
  `has_wifi` tinyint(1) NOT NULL,
  `has_air_conditional` tinyint(1) NOT NULL,
  `has_tv` tinyint(1) NOT NULL,
  `has_iron` tinyint(1) NOT NULL,
  `has_washing_machine` tinyint(1) NOT NULL,
  `cost` int NOT NULL,
  `desc` varchar(255) NOT NULL,
  `bathroom_num` int NOT NULL,
  `bed_num` int NOT NULL,
  `bedroom_num` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `location_id` (`location_id`),
  CONSTRAINT `rooms_ibfk_1` FOREIGN KEY (`location_id`) REFERENCES `locations` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `users` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `pass_word` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `birth_day` date NOT NULL,
  `role` enum('user','admin') NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `gender` enum('male','female','other') NOT NULL,
  `avatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `comments` (`id`, `room_id`, `user_id`, `content`, `rated`, `created_at`, `updated_at`) VALUES
(1, 1, 3, 'asd', 3, '2023-12-17 10:08:34', '2023-12-24 08:29:48'),
(2, 1, 2, 'asd', 3, '2023-12-17 15:34:00', '2023-12-17 15:34:00'),
(3, 1, 4, 'asd', 3, '2023-12-17 15:47:39', '2023-12-17 15:47:39'),
(5, 1, 3, 'asd', 5, '2023-12-17 16:17:50', '2023-12-17 16:17:50'),
(6, 1, 3, 'asd', 5, '2023-12-17 16:18:45', '2023-12-17 16:18:45'),
(7, 1, 3, 'asd', 5, '2023-12-17 16:18:58', '2023-12-17 16:18:58'),
(8, 1, 3, 'asd', 5, '2023-12-17 16:23:51', '2023-12-17 16:23:51'),
(9, 1, 1, 'asd', 5, '2023-12-17 16:28:55', '2023-12-17 16:28:55'),
(10, 1, 1, 'nothing', 3, '2023-12-24 02:42:58', '2023-12-24 02:44:39'),
(11, 3, 1, 'no comment', 3, '2023-12-24 02:43:53', '2023-12-24 02:43:53');

INSERT INTO `locations` (`id`, `address`, `city`, `country`, `photo`, `updated_at`, `created_at`) VALUES
(1, 'landmark', 'lkj', 'zxc', '1702999615467_flower.jpeg', '2023-12-19 15:26:55', '2023-12-10 10:38:14'),
(2, '456', '456', '456', '', '2023-12-19 10:03:19', '2023-12-19 10:03:19');

INSERT INTO `room_bookings` (`id`, `room_id`, `arrival_date`, `departure_date`, `customer_number`, `user_id`, `updated_at`, `created_at`) VALUES
(1, 1, '2023-12-05 04:07:19', '2023-12-13 04:07:36', 3, 1, '2023-11-22 04:07:44', '2023-11-14 04:07:51'),
(2, 1, '2023-12-07 00:00:00', '2023-12-09 00:00:00', 4, 1, '2023-12-19 09:40:40', '2023-12-19 09:40:40'),
(6, 1, '2023-12-07 00:00:00', '2023-12-09 00:00:00', 4, 1, '2023-12-24 02:16:07', '2023-12-24 02:16:07'),
(7, 1, '2023-12-07 00:00:00', '2023-12-09 00:00:00', 4, 1, '2023-12-24 02:17:05', '2023-12-24 02:17:05'),
(8, 1, '2023-12-07 00:00:00', '2023-12-09 00:00:00', 4, 1, '2023-12-24 02:17:37', '2023-12-24 02:17:37'),
(9, 1, '2023-12-07 00:00:00', '2023-12-09 00:00:00', 4, 1, '2023-12-24 02:45:37', '2023-12-24 02:45:37'),
(10, 1, '2023-12-07 00:00:00', '2023-12-09 00:00:00', 3, 1, '2023-12-24 02:46:37', '2023-12-24 02:46:37'),
(11, 1, '2023-12-07 00:00:00', '2023-12-09 00:00:00', 1, 1, '2023-12-24 02:49:15', '2023-12-24 02:49:15');

INSERT INTO `rooms` (`id`, `room_name`, `customer_per_room`, `updated_at`, `created_at`, `image`, `location_id`, `has_ironing_board`, `has_pool`, `has_parking`, `has_stove`, `has_wifi`, `has_air_conditional`, `has_tv`, `has_iron`, `has_washing_machine`, `cost`, `desc`, `bathroom_num`, `bed_num`, `bedroom_num`) VALUES
(1, 'A002', 5, '2023-12-23 10:08:34', '2023-12-10 10:37:24', '1703326884558_flower.jpeg', 1, 1, 0, 1, 0, 0, 1, 0, 1, 1, 1200, 'weqoiweo', 2, 2, 2),
(2, 'M003', 2, '2023-12-23 10:32:25', '2023-12-23 10:32:25', '1703405866408_flower.jpeg', 1, 1, 0, 1, 0, 0, 1, 0, 1, 1, 1200, 'weqoiweo', 2, 2, 2),
(3, 'M004', 2, '2023-12-23 10:33:25', '2023-12-23 10:33:25', '', 1, 1, 0, 1, 0, 0, 1, 0, 1, 1, 1200, 'weqoiweo', 2, 2, 2),
(4, 'M005', 2, '2023-12-23 10:33:52', '2023-12-23 10:33:52', '', 1, 1, 0, 1, 0, 0, 1, 0, 1, 1, 1200, 'weqoiweo', 2, 2, 2),
(6, 'M006', 2, '2023-12-23 10:38:49', '2023-12-23 10:38:49', '', 2, 1, 0, 1, 0, 0, 1, 0, 1, 1, 1200, 'weqoiweo', 2, 2, 2),
(8, 'M009', 2, '2023-12-23 10:42:25', '2023-12-23 10:42:25', '', 1, 1, 0, 1, 0, 0, 1, 0, 1, 1, 1200, 'weqoiweo', 2, 2, 2),
(9, 'M007', 2, '2023-12-24 02:28:04', '2023-12-24 02:28:04', '', 2, 1, 0, 1, 0, 0, 1, 0, 1, 1, 1200, 'weqoiweo', 2, 2, 2),
(10, 'A005', 4, '2023-12-24 02:40:21', '2023-12-24 02:28:27', '', 1, 1, 0, 1, 0, 0, 1, 0, 1, 1, 1200, 'weqoiweo', 2, 2, 2),
(11, 'M011', 2, '2023-12-24 02:32:56', '2023-12-24 02:32:56', '', 2, 1, 0, 1, 0, 0, 1, 0, 1, 1, 1200, 'weqoiweo', 2, 2, 2),
(12, 'M012', 2, '2023-12-24 02:34:01', '2023-12-24 02:34:01', '', 2, 1, 0, 1, 0, 0, 1, 0, 1, 1, 1200, 'weqoiweo', 2, 2, 2),
(13, 'M015', 2, '2023-12-24 02:38:48', '2023-12-24 02:38:48', '', 2, 1, 0, 1, 0, 0, 1, 0, 1, 1, 1200, 'weqoiweo', 2, 2, 2);

INSERT INTO `users` (`id`, `name`, `email`, `pass_word`, `phone`, `birth_day`, `role`, `updated_at`, `created_at`, `gender`, `avatar`) VALUES
(1, 'Alice', 'alice123@gmail.com', '$2b$10$5VhziWqZneP5iVB3OWBtDeHJrVx64vvb89PX7TmB8E/5auCMdZumO', '0988765434', '2003-12-07', 'admin', '2023-12-03 04:29:42', '2023-12-11 04:29:50', 'female', '1703405465332_flower.jpeg'),
(2, 'mimi456', 'mimi456@gmail.com', '$2b$10$VDDBwZ7YhhudY9m9r6z/ruGbFRzLtTrReI/3FDOtaLr5KHpgljW/W', '0324231345', '2003-08-08', 'user', '2023-12-24 08:10:25', '2023-12-03 04:29:42', 'female', '1701877841395_flower.jpeg'),
(3, 'mimi123', 'mimi123@gmail.com', '$2b$10$kcsGJxoCIamZfnmWn2g0G.HFpOObRI/YDA9P6dYVkfKahbmcY0o.G', '0987651234', '2003-08-08', 'user', '2023-12-24 07:54:17', '2023-12-08 16:07:21', 'other', ''),
(4, 'string123', 'string12345@gmail.com', '$2b$10$7oYKYSPJWkINAGk3ZWW6N.WDlLAn5J3HTOikzYxbIScKasl9nCdIi', '0987651267', '2003-12-07', 'admin', '2023-12-08 16:12:42', '2023-12-08 16:12:42', 'male', ''),
(9, 'ddnjhandkjas', 'stringsdsad@gmail.com', '$2b$10$0Z8gY/n2FPomL1j0adhqruhRa5NkyCTbASW/m1K.SinrE3LJ.tdZW', '0987652334', '2003-12-07', 'admin', '2023-12-23 09:46:53', '2023-12-23 09:46:53', 'male', ''),
(11, 'hihi', 'hihi890@gmail.com', '$2b$10$wblCu6YY6n12sAS2AvpoY.C0/UTHDnRWkN8eutoHUbZtelVRMt7uy', '0988751123', '2003-12-07', 'user', '2023-12-23 11:04:59', '2023-12-23 11:04:59', 'female', ''),
(12, 'john', 'john987@gmail.com', '$2b$10$dsiMF3pNCjpL0Xld2M1JD.DXVkCFHWcHLQF/z8xfaCS6zqHoXgxj6', '0988759876', '1997-12-07', 'user', '2023-12-24 01:29:46', '2023-12-24 01:29:46', 'male', ''),
(13, 'anna', 'anna098@gmail.com', '$2b$10$3i8MCmttciMedROKF73hHui7djvi3oLug8PFXBU4/TbKj3fV.pyo.', '0988759809', '1994-12-07', 'user', '2023-12-24 01:52:26', '2023-12-24 01:52:26', 'female', ''),
(18, 'string789', 'string789@gmail.com', '$2b$10$Qh/XKhz9gP.4Z7Ku8xXgmuIxoJwbz.VY4c26aOp4A1voxAjrQjqPu', '0987655834', '2001-12-07', 'user', '2023-12-24 02:05:37', '2023-12-24 02:05:37', 'male', ''),
(19, 'robert', 'robert123@gmail.com', '$2b$10$iJTAn0dXDhtkp1pE75.w4.AmCAt2Q5gCPWmr2DqmwbPpb8tCmW1Cu', '0838759876', '1992-12-07', 'user', '2023-12-24 07:51:55', '2023-12-24 07:51:55', 'male', ''),
(21, 'string567', 'string567@gmail.com', '$2b$10$4C5daL5El/o3oficQdZpqOwgUtM/uf/vOK8/TAbtnDxgLkIq9I6UC', '0377651234', '2003-12-07', 'admin', '2023-12-24 07:57:56', '2023-12-24 07:57:56', 'female', ''),
(23, 'enna', 'enna123@gmail.com', '$2b$10$hbFzF2sH4xm2c20GFDICEuN5WbKykiYmWWHZsd6orG8BS/dOvh9Kq', '0988750987', '1991-12-07', 'user', '2023-12-24 08:07:57', '2023-12-24 08:07:57', 'female', '');



/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;