import { Module } from "@nestjs/common";
import { UserModule } from "./user/user.module";
import { RoomModule } from "./room/room.module";
import { ConfigModule } from "@nestjs/config";
import { PrismaModule } from "./prisma/prisma.module";
import { AuthController } from "./auth/auth.controller";
import { AuthModule } from "./auth/auth.module";
import { PassportModule } from "@nestjs/passport";
import { CommentModule } from './comment/comment.module';
import { LocationModule } from './location/location.module';
import { BookingModule } from './booking/booking.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
    }),
    UserModule,
    RoomModule,
    PassportModule,
    PrismaModule,
    AuthModule,
    CommentModule,
    LocationModule,
    BookingModule,
  ],
})
export class AppModule {}
