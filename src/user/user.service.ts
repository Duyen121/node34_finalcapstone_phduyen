import {
  HttpException,
  Injectable,
  NotFoundException,
  UnauthorizedException,
} from "@nestjs/common";
import { PrismaService } from "../prisma/prisma.service";
import { User } from "./entities/user.entity";
import * as bcrypt from "bcrypt";

@Injectable()
export class UserService {
  constructor(private prisma: PrismaService) {}

  async findAll(
    searchKeyword: string,
    pageSize: number,
    pageIndex: number
  ): Promise<User[]> {
    let data: User[] = await this.prisma.users.findMany({
      where: searchKeyword?.trim() && {
        OR: [
          {
            name: {
              contains: searchKeyword,
            },
          },
          {
            email: {
              contains: searchKeyword,
            },
          },
          {
            phone: {
              contains: searchKeyword,
            },
          },
        ],
      },
      skip: pageSize * (pageIndex - 1) || undefined,
      take: pageSize || undefined,
    });

    return data;
  }

  async findOne(id: number): Promise<User> {
    let user: User = await this.prisma.users.findUnique({ where: { id } });
    return user;
  }

  async isExisted({ email, phone }: User): Promise<User> {
    let user: User = await this.prisma.users.findFirst({
      where: {
        OR: [{ email }, { phone }],
      },
    });
    return user;
  }

  async create(data: any): Promise<User> {
    let hashedPassword: string = await bcrypt.hash(data.pass_word, 10);
    let user: any = await this.prisma.users.create({
      data: { ...data, pass_word: hashedPassword },
    });
    return user;
  }

  async update(id: number, data: any): Promise<User> {
    let user: User = await this.prisma.users.findUnique({
      where: { id },
    });
    if (!user) {
      throw new HttpException("Not existed user!", 422);
    }
    let updatedUser = await this.prisma.users.update({
      where: { id },
      data,
    });
    return updatedUser;
  }

  async remove(id: number): Promise<User | Error> {
    let isExisted: User = await this.findOne(id);
    if (isExisted) {
      let user: User = await this.prisma.users.delete({ where: { id } });
      return user;
    } else throw new NotFoundException();
  }
}
