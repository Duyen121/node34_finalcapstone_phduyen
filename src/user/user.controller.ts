import {
  Body,
  Controller,
  Delete,
  Get,
  HttpException,
  InternalServerErrorException,
  Param,
  Post,
  Put,
  Query,
  Req,
  UploadedFile,
  UseGuards,
  UseInterceptors,
} from "@nestjs/common";
import { UserService } from "./user.service";
import { ApiBody, ApiConsumes, ApiQuery, ApiTags } from "@nestjs/swagger";
import { instanceToPlain, plainToClass } from "class-transformer";
import { User } from "./entities/user.entity";
import { UpdateUserDto } from "./dto/update-user.dto";
import { FileInterceptor } from "@nestjs/platform-express";
import { CheckExistencePipe } from "src/pipes/check-existence.pipe";
import { CompressImagePipe } from "src/pipes/compress-image.pipe";
import { CreateUserDto } from "./dto/create-user.dto";
import { UploadAvatarDto } from "./dto/upload-avatar.dto";
import { LocalAuthGuard } from "src/auth/guards/local-auth.guard";
import { Roles } from "src/auth/decorators/role.decorator";
import { UserRole } from "./user.enum";

@UseGuards(LocalAuthGuard)
@ApiTags("User")
@Controller("user")
export class UserController {
  constructor(private readonly userService: UserService) {}

  @ApiQuery({ name: "id", required: false })
  @ApiQuery({ name: "search", required: false })
  @ApiQuery({ name: "pageSize", required: false })
  @ApiQuery({ name: "pageIndex", required: false })
  @Get()
  async get(
    @Query("id") id: string,
    @Query("search") searchKeyword: string,
    @Query("pageSize") pageSize: string,
    @Query("pageIndex") pageIndex: string
  ) {
    try {
      let data: any;

      if (+id) {
        data = await this.userService.findOne(+id);
      } else {
        data = await this.userService.findAll(
          searchKeyword,
          +pageSize,
          +pageIndex
        );
      }
      if (data?.length === 0 || !data) {
        throw new HttpException("No result found", 200);
      } else {
        return {
          message: "Successfully!",
          data: () =>
          instanceToPlain(
            plainToClass(User, data)
          ),
        };
      }
    } catch (err) {
      throw err || new InternalServerErrorException();
    }
  }

  @Roles(UserRole.ADMIN)
  @ApiConsumes("multipart/form-data")
  @ApiBody({
    type: User,
  })
  @Post("new-user")
  async create(@Body(CheckExistencePipe) data: CreateUserDto) {
    try {
      let user = await this.userService.create({
        ...data,
        avatar: "",
      });

      return {
        message: "Successfully created!",
        data: () =>
          instanceToPlain(
            plainToClass(User, user, )
          ),
      };
    } catch (err) {
      throw err || new InternalServerErrorException();
    }
  }

  @ApiConsumes("multipart/form-data")
  @ApiBody({
    type: UpdateUserDto,
  })
  @Put("update/:id")
  async update(@Param("id") id: string, @Body() data: UpdateUserDto) {
    try {
      let updatedUser: User = await this.userService.update(+id, {
        ...data,
      });

      return {
        message: "Successfully updated!",
        data: () =>
          instanceToPlain(plainToClass(User, updatedUser))
      };
    } catch (err) {
      throw err || new InternalServerErrorException();
    }
  }

  @ApiConsumes("multipart/form-data")
  @ApiBody({
    type: UploadAvatarDto,
  })
  @UseInterceptors(FileInterceptor("avatar"))
  @Post("upload-avatar")
  async uploadAvatar(
    @Req() req,
    @UploadedFile(CompressImagePipe) file: Express.Multer.File
  ) {
    let data: User = req.user.data;
    let userId = data.id;
    if (!file) {
      throw new HttpException("Please upload your avatar!", 422);
    }
    await this.userService.update(+userId, {
      ...data,
      avatar: data?.avatar !== null ? file && file?.filename : null,
    });
    return { message: "Successfully uploaded avatar!" };
  }

  @Roles(UserRole.ADMIN)
  @Delete("delete/:id")
  async delete(@Param("id") id: number) {
    try {
      await this.userService.remove(+id);
      return { message: "Successfully deleted!" };
    } catch (err) {
      throw err || new InternalServerErrorException();
    }
  }
}
