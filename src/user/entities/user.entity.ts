import { Expose } from 'class-transformer';

export class User {
  id?: number;

  avatar?: string;
  name: string;
  email: string;

  @Expose({ groups: ['admin'] })
  pass_word: string;

  phone: string;
  birth_day: Date;
  gender: 'male' | 'female' | 'other';

  @Expose({ groups: ['admin'] })
  role?: 'user' | 'admin';
}
