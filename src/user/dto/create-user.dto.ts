import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import {
  IsDateString,
  IsEmail,
  IsEnum,
  IsNotEmpty,
  IsNumberString,
  IsString,
  Matches,
} from 'class-validator';
import { UserGender, UserRole } from '../user.enum';

export class CreateUserDto {
  @ApiPropertyOptional({ type: 'string', format: 'binary' })
  avatar?: string;

  @ApiProperty({ default: 'name' })
  @IsNotEmpty()
  @Matches(/\D+/g, { message: 'Invalid name!' })
  name: string;

  @ApiProperty({ default: 'username123@gmail.com' })
  @IsNotEmpty()
  @IsEmail(undefined, { message: 'Invalid email!' })
  email: string;

  @ApiProperty()
  @IsNotEmpty()
  pass_word: string;

  @ApiProperty({ default: '0912345678' })
  @IsNotEmpty()
  @IsNumberString(undefined, {message: 'Invalid phone number'})
  phone: string;

  @ApiProperty({ default: new Date().toISOString() })
  @IsNotEmpty()
  @IsDateString(undefined, { message: 'Invalid date' })
  birth_day: Date;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  @IsEnum(UserGender)
  gender: UserGender;

  @ApiProperty({ enum: ['user', 'admin'] })
  @IsNotEmpty()
  @IsString()
  @IsEnum(UserRole)
  role: UserRole;
}