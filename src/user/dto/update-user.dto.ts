import { PartialType } from "@nestjs/mapped-types";
import { CreateUserDto } from "./create-user.dto";
import { ApiPropertyOptional } from "@nestjs/swagger";
import {
  IsDateString,
  IsEmail,
  IsEnum,
  IsNotEmpty,
  IsNumberString,
  IsString,
  Matches,
} from "class-validator";
import { UserGender, UserRole } from "../user.enum";

export class UpdateUserDto extends PartialType(CreateUserDto) {
  @ApiPropertyOptional({ default: "name" })
  @IsNotEmpty()
  @Matches(/\D+/g, { message: "Invalid name!" })
  name: string;

  @ApiPropertyOptional({ default: "username123@gmail.com" })
  @IsNotEmpty()
  @IsEmail(undefined, { message: "Invalid email!" })
  email: string;

  @ApiPropertyOptional({ default: "0912345678" })
  @IsNotEmpty()
  @IsNumberString(undefined, { message: "Invalid phone number" })
  phone: string;

  @ApiPropertyOptional({ default: new Date().toISOString() })
  @IsNotEmpty()
  @IsDateString(undefined, { message: "Invalid date" })
  birth_day: Date;

  @ApiPropertyOptional()
  @IsNotEmpty()
  @IsString()
  @IsEnum(UserGender)
  gender: UserGender;

  @ApiPropertyOptional()
  @IsString()
  @IsNotEmpty()
  @IsEnum(UserRole)
  role: UserRole;
}
