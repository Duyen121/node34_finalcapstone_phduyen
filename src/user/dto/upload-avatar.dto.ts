import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty } from "class-validator";

export class UploadAvatarDto {
  @ApiProperty({ type: String, format: "binary" })
  @IsNotEmpty()
  avatar: any;
}
