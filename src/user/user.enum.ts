export enum UserRole {
  USER = "user",
  ADMIN = "admin",
}

export enum UserGender {
  MALE = "male",
  FEMALE = "female",
  OTHER = "other",
}
