import { Module } from '@nestjs/common';
import { UserService } from './user.service';
import { UserController } from './user.controller';
import { LocalStrategy } from 'src/auth/strategies/local.strategy';
import { APP_GUARD } from '@nestjs/core';
import { UserRolesGuard } from 'src/auth/guards/user-role.guard';
import { JwtService } from '@nestjs/jwt';
import { MulterModule } from '@nestjs/platform-express';
import { diskStorage } from 'multer';

@Module({
  imports: [
    MulterModule.register({
      storage: diskStorage({
        destination: process.cwd() + "/public/img/user-avatar",
        filename: (req, file, callback) =>
          callback(null, new Date().getTime() + "_" + file.originalname),
      }),
    }),
  ],
  controllers: [UserController],
  providers: [UserService, LocalStrategy, {
    provide: APP_GUARD,
    useClass: UserRolesGuard,
  }, JwtService]
})
export class UserModule {}
