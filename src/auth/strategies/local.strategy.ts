import { PassportStrategy } from '@nestjs/passport';
import {
  Injectable,
  InternalServerErrorException,
  UnauthorizedException,
} from '@nestjs/common';
import { Strategy, ExtractJwt } from 'passport-jwt';
import { UserService } from '../../user/user.service';

@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy, 'local') {
  constructor(private readonly userService: UserService) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: true,
      secretOrKey: process.env.SECRET_KEY,
    });
  }

  async validate(decodedToken: any) {
    try {
      const { id } = decodedToken.data;
      const validatedUser = await this.userService.findOne(id);
      if (validatedUser) {
        return decodedToken;
      } else {
        throw new UnauthorizedException();
      }
    } catch (err) {
      throw err || new InternalServerErrorException();
    }
  }
}
