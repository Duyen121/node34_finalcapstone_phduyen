import {
  ForbiddenException,
  Injectable,
} from "@nestjs/common";
import { ConfigService } from "@nestjs/config";
import { JwtService } from "@nestjs/jwt";
import { PrismaService } from "src/prisma/prisma.service";
import { UserService } from "src/user/user.service";
import * as bcrypt from "bcrypt";
import { SignInDto } from "./dto/sign-in.dto";
import { SignUpDto } from "./dto/sign-up.dto";

@Injectable()
export class AuthService {
  constructor(
    private prisma: PrismaService,
    private jwtService: JwtService,
    private userService: UserService,
    private configService: ConfigService
  ) {}

  async signIn(body: SignInDto): Promise<any> {
    let { email, pass_word } = body;
    let user = await this.prisma.users.findFirst({
      where: { email },
    });
    if (!user) {
      throw new ForbiddenException("Credentials incorrect");
    }
    let pwMatches = await bcrypt.compare(pass_word, user.pass_word);
    if (!pwMatches) {
      throw new ForbiddenException("Credentials incorrect");
    }

    let data = this.jwtService.sign(
      { data: user },
      { expiresIn: "1h", secret: this.configService.get("SECRET_KEY") }
    );
    return { message: "Successfully signed in!", accessToken: data };
  }

  async signUp(body: SignUpDto) {
    let user = await this.userService.create({
        ...body,
        role: "user",
        avatar: ""
      });
      return {
        message: "Sign up successfully!",
      };
  }
}
