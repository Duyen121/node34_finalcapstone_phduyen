import { Injectable, CanActivate, ExecutionContext } from "@nestjs/common";
import { Reflector } from "@nestjs/core";
import { UserRole } from "src/user/user.enum";
import { ROLES_KEY } from "../decorators/role.decorator";
import { JwtService } from "@nestjs/jwt";

@Injectable()
export class UserRolesGuard implements CanActivate {
  constructor(
    private reflector: Reflector,
    private jwtService: JwtService
  ) {}

  async matchRoles(roles: string[], userRole: UserRole) {
    let match = false;

    if (roles.indexOf(userRole) > -1) {
      match = true;
    }
    return match
  }

  canActivate(context: ExecutionContext): boolean | Promise<boolean>  {
    const requiredRoles = this.reflector.getAllAndOverride<UserRole[]>(
      ROLES_KEY,
      [context.getHandler(), context.getClass()]
    );
    if (!requiredRoles) {
      return true;
    }

    const req = context.switchToHttp().getRequest();
    const authorization = req.headers["authorization"];
    const [_, token] = authorization.split(" ");
    const user = this.jwtService.decode(token);
    
    return this.matchRoles(requiredRoles, user.data.role);    
  }
}
