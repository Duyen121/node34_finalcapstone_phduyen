import {
  Body,
  Controller,
  HttpCode,
  HttpStatus,
  InternalServerErrorException,
  Post,
} from "@nestjs/common";
import { AuthService } from "./auth.service";
import { SignInDto } from "./dto/sign-in.dto";
import { ApiBody, ApiConsumes } from "@nestjs/swagger";
import { SignUpDto } from "./dto/sign-up.dto";
import { CheckExistencePipe } from "src/pipes/check-existence.pipe";

@Controller("auth")
export class AuthController {
  constructor(
    private authService: AuthService,
  ) {}

  @HttpCode(HttpStatus.OK)
  @Post("sign-in")
  signIn(@Body() body: SignInDto) {
    return this.authService.signIn(body);
  }

  @ApiConsumes("multipart/form-data")
  @ApiBody({
    type: SignUpDto,
  })
  @Post("sign-up")
  async signUp(@Body(CheckExistencePipe) body: SignUpDto) {
    try {
      return this.authService.signUp(body)
    } catch (err) {
      throw err || new InternalServerErrorException();
    }
  }
}
