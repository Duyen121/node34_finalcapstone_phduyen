import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import {
  IsDateString,
  IsEmail,
  IsEnum,
  IsNotEmpty,
  IsNumberString,
  IsString,
  Matches,
} from 'class-validator';
import { UserGender, UserRole } from 'src/user/user.enum';

export class SignUpDto {
  @ApiPropertyOptional({ type: 'string', format: 'binary' })
  avatar?: string;

  @ApiProperty({ default: 'name' })
  @Matches(/\D+/g, { message: 'Invalid name!' })
  @IsNotEmpty()
  name: string;

  @ApiProperty({ default: 'username123@gmail.com' })
  @IsEmail(undefined, { message: 'Invalid email!' })
  @IsNotEmpty()
  email: string;

  @ApiProperty()
  @IsNotEmpty()
  pass_word: string;

  @ApiProperty({ default: '0912345678' })
  @IsNumberString(undefined, {message: 'Invalid phone number'})
  @IsNotEmpty()
  phone: string;

  @ApiProperty({ default: new Date().toISOString() })
  @IsDateString(undefined, { message: 'Invalid date' })
  @IsNotEmpty()
  birth_day: Date;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  @IsEnum(UserGender)
  gender: UserGender;

  @ApiProperty({ enum: ['user', 'admin'] })
  @IsNotEmpty()
  @IsString()
  @IsEnum(UserRole)
  role: UserRole;
}