import { SetMetadata } from '@nestjs/common';
import { UserRole } from '../../user/user.enum';

export const ROLES_KEY = process.env.ROLE_KEY;
export const Roles = (...roles: UserRole[]) => SetMetadata(ROLES_KEY, roles);