import {
  Body,
  Controller,
  Delete,
  Get,
  HttpException,
  InternalServerErrorException,
  Param,
  Post,
  Put,
  Query,
  Req,
  UploadedFile,
  UseGuards,
  UseInterceptors,
} from "@nestjs/common";
import { LocationService } from "./location.service";
import { ApiBody, ApiConsumes, ApiQuery, ApiTags } from "@nestjs/swagger";
import { CheckExistencePipe } from "src/pipes/check-existence.pipe";
import { CreateLocationDto } from "./dto/create-location.dto";
import { Location } from "./entities/location.entities";
import { instanceToPlain, plainToClass } from "class-transformer";
import { UpdateLocationDto } from "./dto/update-location.dto";
import { FileInterceptor } from "@nestjs/platform-express";
import { CompressImagePipe } from "src/pipes/compress-image.pipe";
import { LocalAuthGuard } from "src/auth/guards/local-auth.guard";
import { Roles } from "src/auth/decorators/role.decorator";
import { UserRole } from "src/user/user.enum";

@UseGuards(LocalAuthGuard)
@ApiTags("Location")
@Controller("location")
export class LocationController {
  constructor(private readonly locationService: LocationService) {}

  @ApiQuery({ name: "id", required: false })
  @ApiQuery({ name: "search", required: false })
  @ApiQuery({ name: "pageSize", required: false })
  @ApiQuery({ name: "pageIndex", required: false })
  @Get()
  async get(
    @Query("id") id: string,
    @Query("search") searchKeyword: string,
    @Query("pageSize") pageSize: string,
    @Query("pageIndex") pageIndex: string
  ) {
    try {
      let data: any;

      if (id) {
        data = await this.locationService.findOne(+id);
      } else {
        data = await this.locationService.findAll(
          searchKeyword,
          +pageSize,
          +pageIndex
        );
      }

      if (data?.length === 0 || !data) {
        throw new HttpException("No result found", 200);
      } else {
        return {
          message: "Successfully!",
          data: instanceToPlain(plainToClass(Location, data)),
        };
      }
    } catch (err) {
      throw err || new InternalServerErrorException();
    }
  }

  @Roles(UserRole.ADMIN)
  @ApiConsumes("multipart/form-data")
  @ApiBody({
    type: Location,
  })
  @UseInterceptors(FileInterceptor("photo"))
  @Post("new-location")
  async create(
    @Body(CheckExistencePipe) data: CreateLocationDto,
    @UploadedFile(CompressImagePipe) file: Express.Multer.File
  ) {
    try {
      let location = await this.locationService.create({
        ...data,
        photo: data?.photo !== null ? file && file?.filename : null,
      });
      return {
        message: "Successfully created!",
        data: instanceToPlain(plainToClass(CreateLocationDto, location), {}),
      };
    } catch (err) {
      throw err || new InternalServerErrorException();
    }
  }

  @Roles(UserRole.ADMIN)
  @ApiConsumes("multipart/form-data")
  @ApiBody({
    type: UpdateLocationDto,
  })
  @UseInterceptors(FileInterceptor("photo"))
  @Put("update/:id")
  async update(
    @Param("id") id: string,
    @Body() data: UpdateLocationDto,
    @UploadedFile(CompressImagePipe) file: Express.Multer.File
  ) {
    try {
      const updatedLocation: UpdateLocationDto =
        await this.locationService.update(+id, {
          ...data,
          photo: data?.photo !== null ? file && file?.filename : null,
        });

      return {
        message: "Successfully updated!",
        data: updatedLocation,
      };
    } catch (err) {
      throw err || new InternalServerErrorException();
    }
  }

  @Roles(UserRole.ADMIN)
  @Delete("delete/:id")
  async delete(@Param("id") id: number) {
    try {
      await this.locationService.remove(+id);
      return { message: "Successfully deleted!" };
    } catch (err) {
      throw err || new InternalServerErrorException();
    }
  }
}
