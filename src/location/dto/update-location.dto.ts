import {ApiPropertyOptional } from '@nestjs/swagger';

export class UpdateLocationDto {
  @ApiPropertyOptional()
  address: string;

  @ApiPropertyOptional()
  city: string;

  @ApiPropertyOptional()
  country: string;

  @ApiPropertyOptional({ type: 'string', format: 'binary' })
  photo?: string;
}