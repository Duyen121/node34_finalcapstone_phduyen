import { Module } from '@nestjs/common';
import { LocationService } from './location.service';
import { LocationController } from './location.controller';
import { LocalStrategy } from 'src/auth/strategies/local.strategy';
import { UserService } from 'src/user/user.service';
import { MulterModule } from '@nestjs/platform-express';
import { diskStorage } from 'multer';


@Module({
  imports: [
    MulterModule.register({
      storage: diskStorage({
        destination: process.cwd() + "/public/img/location-photo",
        filename: (req, file, callback) =>
          callback(null, new Date().getTime() + "_" + file.originalname),
      }),
    }),
  ],
  controllers: [LocationController],
  providers: [LocationService, LocalStrategy, UserService]
})
export class LocationModule {}
