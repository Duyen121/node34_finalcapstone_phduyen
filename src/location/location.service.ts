import { Injectable } from "@nestjs/common";
import { PrismaService } from "../prisma/prisma.service";
import { Location } from "./entities/location.entities";
import { CreateLocationDto } from "./dto/create-location.dto";

@Injectable()
export class LocationService {
  constructor(private prisma: PrismaService) {}

  async findAll(
    searchKeyword: string,
    pageSize: number,
    pageIndex: number
  ): Promise<Location[]> {
    const data: Location[] = await this.prisma.locations.findMany({
      where: {
        OR: searchKeyword?.trim()
          ? [
              { address: { contains: searchKeyword || undefined } },
              { city: { contains: searchKeyword || undefined } },
              { country: { contains: searchKeyword || undefined } },
            ]
          : undefined,
      },
      skip: pageSize * (pageIndex - 1) || undefined,
      take: pageSize || undefined,
    });
    return data;
  }

  async findOne(id: number): Promise<Location> {
    const location: Location = await this.prisma.locations.findUnique({
      where: { id },
    });
    return location;
  }

  async create(data: any): Promise<CreateLocationDto> {
    const location: any = await this.prisma.locations.create({ data });
    return location;
  }

  async update(id: number, data: any): Promise<Location> {
    const location: any = await this.prisma.locations.update({
      where: { id },
      data,
    });
    return location;
  }

  async remove(id: number): Promise<Location> {
    const location: Location = await this.prisma.locations.delete({
      where: { id },
    });
    return location;
  }
}
