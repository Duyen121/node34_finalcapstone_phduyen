import { Module } from '@nestjs/common';
import { RoomController } from './room.controller';
import { RoomService } from './room.service';
import { MulterModule } from '@nestjs/platform-express';
import { diskStorage } from 'multer';

@Module({
  imports: [
    MulterModule.register({
      storage: diskStorage({
        destination: process.cwd() + "/public/img/room-image",
        filename: (req, file, callback) =>
          callback(null, new Date().getTime() + "_" + file.originalname),
      }),
    }),
  ],
  controllers: [RoomController],
  providers: [RoomService]
})
export class RoomModule {}
