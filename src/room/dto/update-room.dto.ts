import { ApiPropertyOptional } from "@nestjs/swagger";
import { Type } from "class-transformer";

export class UpdateRoomDto {
  @ApiPropertyOptional()
  room_name: string;

  @ApiPropertyOptional()
  @Type(() => Number)
  location_id: number;

  @ApiPropertyOptional()
  @Type(() => Number)
  cost: number;

  @ApiPropertyOptional()
  desc: string;

  @ApiPropertyOptional()
  @Type(() => Number)
  bed_num: number;

  @ApiPropertyOptional()
  @Type(() => Number)
  bathroom_num: number;

  @ApiPropertyOptional()
  @Type(() => Number)
  customer_per_room: number;

  @ApiPropertyOptional()
  @Type(() => Number)
  bedroom_num: number;

  @ApiPropertyOptional({ enum: ["true", "false"] })
  has_washing_machine: "true" | "false";

  @ApiPropertyOptional({ enum: ["true", "false"] })
  has_iron: "true" | "false";

  @ApiPropertyOptional({ enum: ["true", "false"] })
  has_tv: "true" | "false";

  @ApiPropertyOptional({ enum: ["true", "false"] })
  has_air_conditioner: "true" | "false";

  @ApiPropertyOptional({ enum: ["true", "false"] })
  has_wifi: "true" | "false";

  @ApiPropertyOptional({ enum: ["true", "false"] })
  has_stove: "true" | "false";

  @ApiPropertyOptional({ enum: ["true", "false"] })
  has_parking: "true" | "false";

  @ApiPropertyOptional({ enum: ["true", "false"] })
  has_pool: "true" | "false";
}
