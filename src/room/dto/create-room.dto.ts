import { ApiProperty, ApiPropertyOptional } from "@nestjs/swagger";
import { Type } from "class-transformer";
import { IsNotEmpty } from "class-validator";

export class CreateRoomDto {
  @ApiProperty()
  @IsNotEmpty()
  room_name: string;

  @ApiProperty()
  @Type(() => Number)
  @IsNotEmpty()
  location_id: number;

  @ApiProperty()
  @Type(() => Number)
  @IsNotEmpty()
  cost: number;

  @ApiProperty()
  @IsNotEmpty()
  desc: string;

  @ApiProperty()
  @Type(() => Number)
  @IsNotEmpty()
  bed_num: number;

  @ApiProperty()
  @Type(() => Number)
  @IsNotEmpty()
  bathroom_num: number;

  @ApiProperty()
  @Type(() => Number)
  @IsNotEmpty()
  customer_per_room: number;

  @ApiProperty()
  @Type(() => Number)
  @IsNotEmpty()
  bedroom_num: number;

  @ApiProperty({ enum: ["true", "false"] })
  @IsNotEmpty()
  has_washing_machine: "true" | "false";

  @ApiProperty({ enum: ["true", "false"] })
  @IsNotEmpty()
  has_iron: "true" | "false";

  @ApiProperty({ enum: ["true", "false"] })
  @IsNotEmpty()
  has_tv: "true" | "false";

  @ApiProperty({ enum: ["true", "false"] })
  @IsNotEmpty()
  has_air_conditioner: "true" | "false";

  @ApiProperty({ enum: ["true", "false"] })
  @IsNotEmpty()
  has_wifi: "true" | "false";

  @ApiProperty({ enum: ["true", "false"] })
  @IsNotEmpty()
  has_stove: "true" | "false";

  @ApiProperty({ enum: ["true", "false"] })
  @IsNotEmpty()
  has_parking: "true" | "false";

  @ApiProperty({ enum: ["true", "false"] })
  @IsNotEmpty()
  has_pool: "true" | "false";
}
