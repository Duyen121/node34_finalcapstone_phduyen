import { ApiPropertyOptional } from "@nestjs/swagger";
import { IsNotEmpty } from "class-validator";

export class UploadImageDto {
  @ApiPropertyOptional({ type: String, format: "binary" })
  @IsNotEmpty()
  image: any;
}