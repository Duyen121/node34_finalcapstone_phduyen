import {
  Body,
  Controller,
  Delete,
  Get,
  HttpException,
  InternalServerErrorException,
  Param,
  Post,
  Put,
  Query,
  Req,
  UploadedFile,
  UseGuards,
  UseInterceptors,
} from "@nestjs/common";
import { RoomService } from "./room.service";
import { ApiBody, ApiConsumes, ApiQuery, ApiTags } from "@nestjs/swagger";
import { instanceToPlain, plainToClass } from "class-transformer";
import { Room } from "./entities/room.entity";
import { CreateRoomDto } from "./dto/create-room.dto";
import { UpdateRoomDto } from "./dto/update-room.dto";
import { LocalAuthGuard } from "src/auth/guards/local-auth.guard";
import { FileInterceptor } from "@nestjs/platform-express";
import { CompressImagePipe } from "src/pipes/compress-image.pipe";
import { Roles } from "src/auth/decorators/role.decorator";
import { UserRole } from "src/user/user.enum";
import { UploadImageDto } from "./dto/upload-image.dto";

@UseGuards(LocalAuthGuard)
@ApiTags("Room")
@Controller("room")
export class RoomController {
  constructor(private readonly roomService: RoomService) {}

  @ApiQuery({ name: "id", required: false })
  @ApiQuery({ name: "locationId", required: false })
  @ApiQuery({ name: "search", required: false })
  @ApiQuery({ name: "pageSize", required: false })
  @ApiQuery({ name: "pageIndex", required: false })
  @Get()
  async get(
    @Query("id") id: string,
    @Query("locationId") locationId: string,
    @Query("search") searchKeyword: string,
    @Query("pageSize") pageSize: string,
    @Query("pageIndex") pageIndex: string
  ) {
    try {
      let data: any;

      if (id) {
        data = await this.roomService.findOne(+id);
      } else {
        data = await this.roomService.findAll(
          +locationId,
          searchKeyword,
          +pageSize,
          +pageIndex
        );
      }

      if (data?.length === 0 || !data) {
        throw new HttpException("No result found", 200);
      } else {
        return {
          message: "Successfully!",
          data: instanceToPlain(plainToClass(Room, data)),
        };
      }
    } catch (err) {
      throw err || new InternalServerErrorException();
    }
  }

  @Roles(UserRole.ADMIN)
  @ApiConsumes("multipart/form-data")
  @ApiBody({
    type: CreateRoomDto,
  })
  @Post("new-room")
  async create(@Body() data: CreateRoomDto) {
    try {
      let room: CreateRoomDto = await this.roomService.create({
        ...data,
        image: "",
      });
      return {
        message: "Successfully created!",
        data: instanceToPlain(plainToClass(Room, room))
      };
    } catch (err) {
      throw err || new InternalServerErrorException();
    }
  }

  @Roles(UserRole.ADMIN)
  @ApiConsumes("multipart/form-data")
  @ApiBody({
    type: UpdateRoomDto,
  })
  @Put("update/:id")
  async update(@Param("id") id: string, @Body() data: UpdateRoomDto) {
    try {
      let room = await this.roomService.update(+id, {
        ...data,
        image: "",
      });
      return {
        message: "Successfully updated!",
        data: instanceToPlain(plainToClass(Room, room)),
      };
    } catch (err) {
      throw err || new InternalServerErrorException();
    }
  }

  @Roles(UserRole.ADMIN)
  @ApiConsumes("multipart/form-data")
  @ApiQuery({ name: "roomId", required: true })
  @ApiBody({
    type: UploadImageDto,
  })
  @UseInterceptors(FileInterceptor("image"))
  @Post("upload-image")
  async uploadAvatar(
    @Query("roomId") roomId: string,
    @UploadedFile(CompressImagePipe) file: Express.Multer.File
  ) {
    if (!file) {
      throw new HttpException("Please upload room image!", 422);
    }
    let data = await this.roomService.findOne(+roomId);
    await this.roomService.update(+roomId, {
      ...data,
      image: data?.image !== null ? file && file?.filename : null,
    });
    return { message: "Successfully uploaded room image!" };
  }

  @Roles(UserRole.ADMIN)
  @Delete("delete/:id")
  async delete(@Param("id") id: string) {
    try {
      await this.roomService.remove(+id);
      return { message: "Successfully deleted!" };
    } catch (err) {
      throw err || new InternalServerErrorException();
    }
  }
}
