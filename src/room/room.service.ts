import { Injectable } from "@nestjs/common";
import { PrismaService } from "src/prisma/prisma.service";
import { Room } from "./entities/room.entity";
import { CreateRoomDto } from "./dto/create-room.dto";
import { UpdateRoomDto } from "./dto/update-room.dto";

@Injectable()
export class RoomService {
  constructor(private prisma: PrismaService) {}

  async findAll(
    location_id: number,
    searchKeyword: string,
    pageSize: number,
    pageIndex: number
  ): Promise<Room[]> {
    let data: any = await this.prisma.rooms.findMany({
      where: {
        OR: searchKeyword?.trim()
          ? [
              { room_name: { contains: searchKeyword } },
              {
                locations: {
                  OR: [
                    { address: { contains: searchKeyword } },
                    { city: { contains: searchKeyword } },
                    { country: { contains: searchKeyword } },
                  ],
                },
              },
            ]
          : undefined,
        location_id: location_id || undefined,
      },
      include: {
        locations: {
          select: {
            address: true,
            city: true,
            country: true,
          },
        },
      },
      skip: pageSize * (pageIndex - 1) || undefined,
      take: pageSize || undefined,
    });
    return data;
  }

  async findOne(id: number): Promise<Room> {
    let room: any = await this.prisma.rooms.findUnique({
      where: { id },
    });
    return room;
  }

  async create(data: any): Promise<CreateRoomDto> {
    let room: any = await this.prisma.rooms.create({
      data,
      include: {
        locations: {
          select: {
            address: true,
            city: true,
            country: true,
          },
        },
      },
    });
    return room;
  }

  async update(id: number, data: any): Promise<any> {
    let room: any = await this.prisma.rooms.update({
      where: { id },
      data,
      include: {
        locations: {
          select: {
            address: true,
            city: true,
            country: true,
          },
        },
      },
    });
    return room;
  }

  async remove(id: number): Promise<Room> {
    let room: any = await this.prisma.rooms.delete({ where: { id } });
    return room;
  }
}
