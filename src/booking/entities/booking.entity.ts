import { Exclude } from 'class-transformer';
import { User } from 'src/user/entities/user.entity';

export class Booking {
  id?: number;

  @Exclude()
  user_id: number;

  user: User;

  @Exclude()
  room_id: number;

  customer_number: number;

  arrival_date: Date;

  departure_date: Date;
}
