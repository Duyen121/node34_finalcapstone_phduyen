import { ApiPropertyOptional } from "@nestjs/swagger";
import { IsNotEmpty } from "class-validator";

export class UpdateBookingDto {
  @ApiPropertyOptional()
  @IsNotEmpty()
  user_id: number;

  @ApiPropertyOptional()
  @IsNotEmpty()
  room_id: number;

  @ApiPropertyOptional()
  @IsNotEmpty()
  customer_number: number;

  @ApiPropertyOptional()
  @IsNotEmpty()
  arrival_date: Date;

  @ApiPropertyOptional()
  @IsNotEmpty()
  departure_date: Date;
}
