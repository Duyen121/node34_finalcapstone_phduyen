import { ApiProperty } from "@nestjs/swagger";
import { IsDateString, IsInt, IsNotEmpty } from "class-validator";

export class CreateBookingDto {
  @IsInt()
  id?: number;

  @ApiProperty()
  @IsInt()
  @IsNotEmpty()
  user_id: number;

  @ApiProperty()
  @IsInt()
  @IsNotEmpty()
  room_id: number;

  @ApiProperty()
  @IsInt()
  @IsNotEmpty()
  customer_number: number;

  @ApiProperty()
  @IsDateString()
  @IsNotEmpty()
  arrival_date: Date;

  @ApiProperty()
  @IsDateString()
  @IsNotEmpty()
  departure_date: Date;
}