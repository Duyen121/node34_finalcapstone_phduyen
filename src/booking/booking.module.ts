import { Module } from '@nestjs/common';
import { BookingService } from './booking.service';
import { BookingController } from './booking.controller';
import { JwtService } from '@nestjs/jwt';
import { RoomService } from 'src/room/room.service';

@Module({
  controllers:[BookingController],
  providers: [BookingService, JwtService, RoomService]
})
export class BookingModule {}
