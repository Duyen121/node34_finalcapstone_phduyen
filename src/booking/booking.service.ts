import { Injectable } from "@nestjs/common";
import { PrismaService } from "src/prisma/prisma.service";
import { Booking } from "./entities/booking.entity";
import { CreateBookingDto } from "./dto/create-booking.dto";
import { UpdateBookingDto } from "./dto/update-booking.dto";

@Injectable()
export class BookingService {
  constructor(private prisma: PrismaService) {}

  async findAll(
    userId: number,
    searchKeyword: string,
    pageSize: number,
    pageIndex: number
  ): Promise<Booking[]> {
    let data: any[] = await this.prisma.room_bookings.findMany({
      where: {
        OR: searchKeyword?.trim()
          ? [
              {
                users: {
                  OR: [
                    { name: { contains: searchKeyword } },
                    { email: { contains: searchKeyword } },
                    { phone: { contains: searchKeyword } },
                  ],
                },
              },
            ]
          : undefined,
        user_id: userId || undefined,
      },
      include: {
        users: {
          select: {
            name: true,
          },
        },
      },
      skip: pageSize * (pageIndex - 1) || undefined,
      take: pageSize || undefined,
    });
    return data;
  }

  async findOne(id: number): Promise<Booking> {
    let booking: any = await this.prisma.room_bookings.findFirst({
      where: { id },
    });
    return booking;
  }

  async create(data: CreateBookingDto, user: any): Promise<CreateBookingDto> {
    let booking = await this.prisma.room_bookings.create({
      data: { ...data, user_id: user.data.id },
      include: {
        rooms: {
          select: {
            room_name: true,
            locations: {
              select: {
                address: true,
                city: true,
                country: true,
              },
            },
          },
        },
      },
    });
    return booking;
  }

  async update(id: number, data: UpdateBookingDto): Promise<UpdateBookingDto> {
    let booking: any = await this.prisma.room_bookings.update({
      where: { id },
      data,
      include: {
        rooms: {
          select: {
            room_name: true,
            locations: {
              select: {
                address: true,
                city: true,
                country: true
              }
            }
          },
        },
      },
    });
    return booking;
  }

  async remove(id: number): Promise<Booking> {
    let booking: any = await this.prisma.room_bookings.delete({
      where: { id },
    });
    return booking;
  }
}
