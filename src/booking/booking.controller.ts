import {
  Body,
  Controller,
  Delete,
  Get,
  HttpException,
  InternalServerErrorException,
  Param,
  Post,
  Put,
  Query,
  Req,
  UseGuards,
} from "@nestjs/common";
import { BookingService } from "./booking.service";
import { ApiQuery, ApiTags } from "@nestjs/swagger";
import { instanceToPlain, plainToClass } from "class-transformer";
import { Booking } from "./entities/booking.entity";
import { CreateBookingDto } from "./dto/create-booking.dto";
import { UpdateBookingDto } from "./dto/update-booking.dto";
import { LocalAuthGuard } from "src/auth/guards/local-auth.guard";
import { JwtService } from "@nestjs/jwt";
import { RoomService } from "src/room/room.service";

@UseGuards(LocalAuthGuard)
@ApiTags("Booking")
@Controller("booking")
export class BookingController {
  constructor(
    private readonly bookingService: BookingService,
    private readonly roomService: RoomService,
    private jwtService: JwtService
  ) {}

  @ApiQuery({ name: "userId", required: false })
  @ApiQuery({ name: "id", required: false })
  @ApiQuery({ name: "search", required: false })
  @ApiQuery({ name: "pageSize", required: false })
  @ApiQuery({ name: "pageIndex", required: false })
  @Get()
  async get(
    @Query("id") id: string,
    @Query("userId") userId: string,
    @Query("search") searchKeyword: string,
    @Query("pageSize") pageSize: string,
    @Query("pageIndex") pageIndex: string
  ) {
    try {
      let data: any;

      if (id) {
        data = await this.bookingService.findOne(+id);
      } else {
        data = await this.bookingService.findAll(
          +userId,
          searchKeyword,
          +pageSize,
          +pageIndex
        );
      }

      if (data?.length === 0 || !data) {
        throw new HttpException("No result found", 200);
      } else {
        return {
          message: "Successfully!",
          data: instanceToPlain(plainToClass(Booking, data)),
        };
      }
    } catch (err) {
      throw err || new InternalServerErrorException();
    }
  }

  @Post("new-booking")
  async create(@Body() data: CreateBookingDto, @Req() req: Request) {
    try {
      const authorization = req.headers["authorization"];
      const [_, token] = authorization.split(" ");
      let user = this.jwtService.decode(token);
      let room = await this.roomService.findOne(+data.room_id);
      if (!(data.customer_number <= room.customer_per_room)) {
        throw new HttpException("Invalid customer number", 422);
      }
      let booking: CreateBookingDto = await this.bookingService.create(
        data,
        user
      );
      return {
        message: "A new reservation is successfully created!",
        data: instanceToPlain(plainToClass(Booking, booking)),
      };
    } catch (err) {
      throw err || new InternalServerErrorException();
    }
  }

  @Put("update/:id")
  async update(@Param("id") id: string, @Body() data: UpdateBookingDto) {
    try {
      let room = await this.roomService.findOne(+data.room_id);
      if (!(data.customer_number <= room.customer_per_room)) {
        throw new HttpException("Invalid customer number", 422);
      }
      let booking: UpdateBookingDto = await this.bookingService.update(
        +id,
        data
      );
      return { message: "Successfully updated!", data: instanceToPlain(plainToClass(Booking, booking)), };
    } catch (err) {
      throw err || new InternalServerErrorException();
    }
  }

  @Delete("delete/:id")
  async delete(@Param("id") id: string) {
    try {
      await this.bookingService.remove(+id);
      return { message: "Successfully deleted!" };
    } catch (err) {
      throw err || new InternalServerErrorException();
    }
  }
}
