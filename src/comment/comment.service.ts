import { Injectable } from "@nestjs/common";
import { PrismaService } from "src/prisma/prisma.service";
import { CreateCommentDto } from "./dto/create-comment.dto";
import { UpdateCommentDto } from "./dto/update-comment.dto";
import { UserComment } from "./entities/user-comment.entity";

@Injectable()
export class CommentService {
  constructor(private prisma: PrismaService) {}

  async findAll(
    roomId: number,
    pageSize: number,
    pageIndex: number
  ): Promise<UserComment[]> {
    let data: any = await this.prisma.comments.findMany({
      where: { room_id: roomId || undefined },
      include: {
        users: {
          select: {
            name: true,
          },
        },
        rooms: {
          select: {
            room_name: true,
          },
        },
      },
      skip: pageSize * (pageIndex - 1) || undefined,
      take: pageSize || undefined,
    });
    return data;
  }

  async findOne(id: number): Promise<UserComment> {
    let comment: any = await this.prisma.comments.findFirst({
      where: { id },
    });
    return comment;
  }

  async create(data: CreateCommentDto, user: any): Promise<CreateCommentDto> {
    let comment: CreateCommentDto = await this.prisma.comments.create({
      data: { ...data, user_id: user.data.id },
      include: {
        rooms: {
          select: {
            room_name: true,
          },
        },
      },
    });
    return comment;
  }

  async update(id: number, data: UpdateCommentDto): Promise<UpdateCommentDto> {
    let comment: UpdateCommentDto = await this.prisma.comments.update({
      where: { id },
      data,
      include: {
        rooms: {
          select: {
            room_name: true,
          },
        },
      },
    });
    return comment;
  }

  async remove(id: number): Promise<UserComment> {
    let comment: any = await this.prisma.comments.delete({
      where: { id },
    });
    return comment;
  }
}
