import { Exclude } from 'class-transformer';
import { Room } from 'src/room/entities/room.entity';
import { User } from 'src/user/entities/user.entity';


export class UserComment {
  id?: number;

  @Exclude()
  room_id: number;

  @Exclude()
  user_id: number;

  user: User;

  content: string;

  rated: number;
}
