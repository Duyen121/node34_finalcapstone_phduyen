import { ApiPropertyOptional } from "@nestjs/swagger";

export class UpdateCommentDto {
  @ApiPropertyOptional()
  room_id: number;

  @ApiPropertyOptional()
  user_id: number;

  @ApiPropertyOptional()
  content: string;

  @ApiPropertyOptional()
  rated: number;
}
