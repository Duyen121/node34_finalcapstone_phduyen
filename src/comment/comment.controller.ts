import {
  Body,
  Controller,
  Delete,
  Get,
  HttpException,
  InternalServerErrorException,
  Param,
  Post,
  Put,
  Query,
  Req,
  UseGuards,
} from "@nestjs/common";
import { JwtService } from "@nestjs/jwt";
import { CommentService } from "./comment.service";
import { ApiQuery, ApiTags } from "@nestjs/swagger";
import { instanceToPlain, plainToClass } from "class-transformer";
import { CreateCommentDto } from "./dto/create-comment.dto";
import { UpdateCommentDto } from "./dto/update-comment.dto";
import { UserComment } from "./entities/user-comment.entity";
import { LocalAuthGuard } from "src/auth/guards/local-auth.guard";

@UseGuards(LocalAuthGuard)
@ApiTags("Comments")
@Controller("comment")
export class CommentController {
  constructor(
    private readonly commentService: CommentService,
    private jwtService: JwtService
  ) {}

  @ApiQuery({ name: "id", required: false })
  @ApiQuery({ name: "roomId", required: false })
  @ApiQuery({ name: "pageSize", required: false })
  @ApiQuery({ name: "pageIndex", required: false })
  @Get("")
  async get(
    @Query("id") id: string,
    @Query("roomId") roomID: string,
    @Query("pageSize") pageSize: string,
    @Query("pageIndex") pageIndex: string
  ) {
    try {
      let data: any;

      if (id) {
        data = await this.commentService.findOne(+id);
      } else {
        data = await this.commentService.findAll(
          +roomID,
          +pageSize,
          +pageIndex
        );
      }

      if (data?.length === 0 || !data) {
        throw new HttpException("No result found", 200);
      } else {
        return {
          message: "Successfully!",
          data: instanceToPlain(plainToClass(UserComment, data)),
        };
      }
    } catch (err) {
      throw err || new InternalServerErrorException();
    }
  }

  @Post("new-comment")
  async create(@Body() data: CreateCommentDto, @Req() req: Request) {
    try {
      const authorization = req.headers["authorization"];
      const [_, token] = authorization.split(" ");
      let user = this.jwtService.decode(token);

      let comment: CreateCommentDto = await this.commentService.create(
        data,
        user
      );
      return { message: "Successfully posted!", data: instanceToPlain(plainToClass(UserComment, comment)), };
    } catch (err) {
      throw err || new InternalServerErrorException();
    }
  }

  @Put("update/:id")
  async udpate(@Param("id") id: string, @Body() data: UpdateCommentDto) {
    try {
      let comment: UpdateCommentDto = await this.commentService.update(
        +id,
        data
      );
      return { message: "Successfully updated!", data: instanceToPlain(plainToClass(UserComment, comment)), };
    } catch (err) {
      throw err || new InternalServerErrorException();
    }
  }

  @Delete("delete/:id")
  async delete(@Param("id") id: string) {
    try {
      await this.commentService.remove(+id);
      return { message: "Successfully deleted!" };
    } catch (err) {
      throw err || new InternalServerErrorException();
    }
  }
}
