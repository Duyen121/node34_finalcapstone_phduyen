import {
  PipeTransform,
  Injectable,
  ArgumentMetadata,
  BadRequestException,
} from '@nestjs/common';
import { User } from 'src/user/entities/user.entity';
import { UserService } from 'src/user/user.service';

@Injectable()
export class CheckExistencePipe implements PipeTransform {
  constructor(private readonly usersService: UserService) {}

  async transform(user: User, metadata: ArgumentMetadata) {
    if (user.email || user.phone) {
      const isExisted = await this.usersService.isExisted(user);
      if (isExisted) {
        throw new BadRequestException(
          'This email/phone number is already existed!',
        );
      }
    }
    return user;
  }
}

